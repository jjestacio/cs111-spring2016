#include <unistd.h>/*termios(), pipe(), isatty(), read(), write(), exec()*/
#include <termios.h> /*termios()*/
#include <stdio.h> /*print(), getc()*/
#include <stdlib.h> /*exit()*/
#include <signal.h> /*signal()*/
#include <getopt.h> /*getopt_long()*/
#include <sys/types.h> /*wait()*/
#include <sys/wait.h>
#include <pthread.h> /*pthread()*/

static int RC = EXIT_SUCCESS;
static int status;
struct termios orig_term_attr, new_term_attr;

void signal_handler(int signum);
void restore_orig_term_attr(void);
void init_new_term(void);
void *process_shell_output(void *read_from_shell);

int main(int argc, char *argv[]) {

  init_new_term();

  pthread_t thread_from_shell;
  int process_id = -1;
  int option;
  int option_index;
  int shell_on = 0;
  int ISIG_on = 0;

  int pipe_to_shell[2]; /*[0]=read end; [1]=write end*/
  int pipe_from_shell[2];

  static struct option long_options[] = {
    {"shell", no_argument, NULL, 'a'}
  };

  while((option = getopt_long(argc, argv, "", long_options, &option_index)) != EOF)
    switch(option)
    case 'a':
      shell_on = 1;

  if(shell_on) {

    /*Initialize pipes*/
    if(pipe(pipe_to_shell) == -1 || pipe(pipe_from_shell) == -1) {
      fprintf(stderr, "Error: creating pipes.");
      exit(EXIT_FAILURE);
    }

    /*Turn off interrupts*/
    ISIG_on = 1;
    new_term_attr.c_lflag &= ~(ISIG);
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &new_term_attr);

    /*Create child process for shell*/
    if((process_id = fork()) == -1) {
      fprintf(stderr, "Error: could not create child process.");
      exit(EXIT_FAILURE);
    }
  }

  /*---- Terminal -------------------------------*/
  if(shell_on && process_id != 0) {

    signal(SIGINT, signal_handler);
    
    close(pipe_to_shell[0]); /*Close read end to shell*/
    close(pipe_from_shell[1]); /*Close write end from shell*/

    /*Create thread to process output from shell*/
    pthread_create(&thread_from_shell, NULL, &process_shell_output, &pipe_from_shell[0]);
    
  }

  /*---- Shell ---------------------------------*/
  if(process_id == 0) {
    
    close(pipe_to_shell[1]); /*Close write end to terminal*/
    close(pipe_from_shell[0]); /*Close read end to terminal*/
    dup2(pipe_to_shell[0], STDIN_FILENO); /*Make shell's stdin input from terminal*/
    dup2(pipe_from_shell[1], STDOUT_FILENO); /*Make shell'stdout output to terminal*/
    close(pipe_to_shell[0]); /*Close oldfd for shell's stdin*/
    close(pipe_from_shell[1]); /*Close oldfd for shell's stdout*/

    char *shell_args[2];
    shell_args[0] = "/bin/bash";
    shell_args[1] = NULL;
    
    if(execvp(shell_args[0], shell_args) == -1) {
      fprintf(stderr, "Error: could not launch shell.");
      exit(EXIT_FAILURE);
    }
  }

  char c;
  char INT = 0x3;
  char EOT = 0x4;
  char CR = 0xD;
  char LF = 0xA;
  while(process_id != 0) {

    if(read(STDIN_FILENO, &c, 1) != 1)
      fprintf(stderr, "Error: could not read c from stdin.");
    
    if(c == EOT) /*^D*/ {
      if(ISIG_on) {
	/*fprintf(stderr, "Recieved ^D.\n");*/
	kill(process_id, SIGHUP);
	close(pipe_to_shell[1]);
	close(pipe_from_shell[0]);
      }
      break;
    }

    if(ISIG_on && c == INT) {
      /*fprintf(stderr, "Received ^C.\n");*/
      kill(process_id, SIGINT);
      break;
    }

    /*Map <cr> or <lf> to <cr><lf>*/
    if(c == CR || c == LF) { /*<cr> ||  <lf>*/

      if(write(STDOUT_FILENO, &CR, 1) != 1) {
        fprintf(stderr, "Error: could not write <cr> to stdout.");
        exit(EXIT_FAILURE);
      }

      if(write(STDOUT_FILENO, &LF, 1) != 1) {
        fprintf(stderr, "Error: could not write <lf> to stdout.");
        exit(EXIT_FAILURE);
      }
      
      if(shell_on && write(pipe_to_shell[1], &LF, 1) != 1) {
        fprintf(stderr, "Error: could not write <lf> to pipe_to_shell[1].");
        exit(EXIT_FAILURE);
      }
 
      continue;
    }

    /*Write other characters normally*/
    if(write(STDOUT_FILENO, &c, 1) != 1) {
      fprintf(stderr, "Error: could not write c to stdout.");
      exit(EXIT_FAILURE);
    }
    
    if(shell_on && write(pipe_to_shell[1], &c, 1) != 1) {
      fprintf(stderr, "Error: could not write c to pipe_to_shell[1].");
      exit(EXIT_FAILURE);
    }
  }

  if(shell_on) {

    waitpid(process_id, &status, 0);

    if(WIFSIGNALED(status)) {
      fprintf(stderr, "Shell terminated by signal: %d.\n", WTERMSIG(status));
      RC = WEXITSTATUS(status);
    }
  }

  fprintf(stderr, "Exit status: %d\n", RC);
  exit(RC);
  
}

/* -------------------------------------- */
/* ---------Other Functions ------------- */
/* -------------------------------------- */

void signal_handler(int signum) {

  if(signum == SIGINT) {
    RC = 1;
    exit(RC);
  }

}

void restore_orig_term_attr(void) {

  tcsetattr(STDIN_FILENO, TCSANOW, &orig_term_attr);
  /*fprintf(stdout, "Restored original terminal attributes\n.");*/
  
}

void init_new_term(void) {

  if(!isatty(STDIN_FILENO)) {
    fprintf(stderr, "Error: STDIN_FILENO is not a terminal\n.");
    exit(EXIT_FAILURE);
  }

  /*Saving original attributes*/
  if(tcgetattr(STDIN_FILENO, &orig_term_attr) == -1) {
    fprintf(stderr, "Error: could not get original terminal attributes\n.");
    exit(EXIT_FAILURE);
  }

  /*Preparing to set new attributes*/
  if(tcgetattr(STDIN_FILENO, &new_term_attr) == -1) {
    fprintf(stderr, "Error: could not get new terminal attributes\n.");
    exit(EXIT_FAILURE);
  }

  /*local modes; non-canonical, no echo input*/
  new_term_attr.c_lflag &= ~(ICANON|ECHO);
  new_term_attr.c_cc[VMIN] = 1;

  if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &new_term_attr) == -1) {
    fprintf(stderr, "Error: could not set new terminal attributes\n.");
    exit(EXIT_FAILURE);
  }

  atexit(restore_orig_term_attr);
}

void *process_shell_output(void *read_from_shell) {

  int fd_read_from_shell = *((int *)read_from_shell);
  
  char c;
  while(1) {

    read(fd_read_from_shell, &c, 1);
    
    if(c == 0) {
      RC = 1;
      break;
    }
    
    write(STDOUT_FILENO, &c, 1);
  }
  
  return NULL;
}
