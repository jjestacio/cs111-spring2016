#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <getopt.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

/*------- lock_type -------------*/
#define NO_LOCK 1
#define MUTEX 2
#define SPIN_LOCK 3
#define COMPARE_AND_SWAP 4

/*------- Constants -----------------------*/
static const int NUM_OPS = 2; // add & subtract

/*------- Global Variables ----------------*/

// Misc
int num_threads = 1;
int num_iterations = 1;
long long counter = 0;

// Lock variables
int opt_yield = 0;
int lock_type = NO_LOCK;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
volatile int spin_lock;


/*------- Other Functions -----------------*/
void add(long long *pointer, long long value);
void *thread_create();
int calc_num_ops(int num_thr, int num_its);

/*------- Main ----------------------------*/
int main (int argc, char* argv[]) {

  /*-------- Get options ---------------------*/

  int option;
  int option_index;
  
  static struct option long_options[] = {
    {"threads", required_argument, NULL, 't'},
    {"iterations", required_argument, NULL, 'i'},
    {"yield", no_argument, NULL, 'y'},
    {"sync", required_argument, NULL, 's'}
  };
    
  while((option = getopt_long(argc, argv, "", long_options, &option_index)) != EOF)
    switch(option) {
      
    case 't': // --threads=optarg
      if((num_threads = atoi(optarg)) == 0 || num_threads <= 0) {
	fprintf(stderr, "ERROR: could not convert num_threads option argument into integer or argument given is <= 0.\n");
	exit(EXIT_FAILURE);
      }
      break;
      
    case 'i': // --iterations=optarg
      if((num_iterations = atoi(optarg)) == 0 || num_iterations <= 0) {
	fprintf(stderr, "ERROR: could not convert num_iterations option argument into integer or argument given is <= 0.\n");
	exit(EXIT_FAILURE);
      }
      break;
      
    case 'y': // --yield
      opt_yield = 1;
      break;
      
    case 's': // --sync
      if(*optarg == 'm') lock_type = MUTEX;
      else if (*optarg == 's') lock_type = SPIN_LOCK;
      else if (*optarg == 'c') lock_type = COMPARE_AND_SWAP;
      else {
	fprintf(stderr, "ERROR: invalid argument for --sync given.\n");
	exit(EXIT_FAILURE);
      }
      break;
    }

  /*---- Execute operations -------*/

  // Start time
  struct timespec start_time;
  clock_gettime(CLOCK_MONOTONIC, &start_time);

  // Create threads
  int i;
  pthread_t thread[num_threads];  

  for(i = 0; i < num_threads; i++)
    if(pthread_create(&thread[i], NULL, &thread_create, NULL) != EXIT_SUCCESS) {
      fprintf(stderr, "ERROR: could not create thread[%d].\n", i);
      exit(EXIT_FAILURE);
    }

  // Join threads
  for(i = 0; i < num_threads; i++)
    if(pthread_join(thread[i], NULL) != EXIT_SUCCESS) {
      fprintf(stderr, "ERROR: could not join thread[%d].\n", i);
      exit(EXIT_FAILURE);
    }

  // End timer
  struct timespec end_time;
  clock_gettime(CLOCK_MONOTONIC, &end_time);

  /*------ Output -------------*/

  int num_ops = num_threads * num_iterations * NUM_OPS;
  fprintf(stdout, "%d threads x %d iterations x (add + subtract) = %d\n", num_threads, num_iterations, num_ops);

  // Checks for preemption
  if(counter != 0)
    fprintf(stderr, "ERROR: final count = %lld\n", counter);

  long long elapsed_time = (end_time.tv_sec - start_time.tv_sec) * 1000000000 + end_time.tv_nsec - start_time.tv_nsec;
  fprintf(stdout, "elapsed time: %lld\n", elapsed_time);

  long long avg_time_per_op = elapsed_time / (long long) num_ops;
  fprintf(stdout, "per operation: %lld\n", avg_time_per_op);

  exit(EXIT_SUCCESS);
}

/*------- Other Functions' Declarations -----------------*/
void add(long long *pointer, long long value) {
  
  long long sum = *pointer + value;
  if (opt_yield)
    pthread_yield();
  *pointer = sum;
}

void *thread_create() {

  int i;
  long long previous = counter;
  long long new_counter = previous + 1;

  /*------- add 1 ----------------*/
  for(i = 0; i < num_iterations; i++)
    switch(lock_type) {
    case NO_LOCK:
      add(&counter, 1);
      break;
    case MUTEX:
      pthread_mutex_lock(&lock);
      add(&counter, 1);
      pthread_mutex_unlock(&lock);
      break;
    case SPIN_LOCK:
      while(__sync_lock_test_and_set(&spin_lock, 1));
      add(&counter, 1);
      __sync_lock_release(&spin_lock);
      break;
    case COMPARE_AND_SWAP:
      do {
	
	previous = counter;
	new_counter = previous + 1;
	
	if(opt_yield)
	  pthread_yield();

      } while(__sync_val_compare_and_swap(&counter, previous, new_counter) != previous);
      break;
    }

  /*------- subtract 1 ----------------*/
  for(i = 0; i < num_iterations; i++)
    switch(lock_type) {
    case NO_LOCK:
      add(&counter, -1);
      break;
    case MUTEX:
      pthread_mutex_lock(&lock);
      add(&counter, -1);
      pthread_mutex_unlock(&lock);
      break;
    case SPIN_LOCK:
      while(__sync_lock_test_and_set(&spin_lock, 1));
      add(&counter, -1);
      __sync_lock_release(&spin_lock);
      break;
    case COMPARE_AND_SWAP:
      do {
	
	previous = counter;
	new_counter = previous - 1;
	
	if(opt_yield)
	  pthread_yield();

      } while(__sync_val_compare_and_swap(&counter, previous, new_counter) != previous);
      break;
    }

  return NULL;
}
