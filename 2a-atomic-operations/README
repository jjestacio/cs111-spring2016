------------------------
INCLUDED FILES

README
Makefile

graph1.png: A plot of the average time per overation vs the number of iterations. Graphed using LibreOffice Calc.

graph2.png: A plot of the average time per operation vs the number of threads for all four versions of the add function. Graphed using LibreOffice Calc.

lab2a.c: Shows how atomic operations work by implementing various different types of mechanisms to give threads exclusive access to a critical section.

------------------------
ANSWERS TO QUESTIONS

QUESTION 2A.1A: Why does it take this many threads or iterations to result in failure?
ANSWER 2A.1A: I found that running lab2a with 2+ threads with 1000+ iterations or 3+ threads with 800+ iterations consistently produced final count errors. The program startings failing at this high of iterations and threads because with more iterations and more threads, there is more chance of preemption occuring before a thread finishes adding and subtracting to the counter. As we add more threads, the chances of preemmption increases. 

QUESTION 2A.1B: Why does a significantly smaller number of iterations so seldom fail?
ANSWER 2A.1B: For any number of threads, significantly smaller number of iterations failed because there is much less chance of threads to interrupt each other. Threads must be kept running longer for higher number of iterations and thus there is more time for them to potentially be interrupted.

QUESTION 2A.2A: Why does the average cost per operation drop with increasing iterations?
ANSWER 2A.2A: The time to execute the add function stays the same. The overhead of the context switches between all the threads is also constant since the number of threads created is constant. In this graph, the cost per operation decreases because the cost per operation is proportional to the total time it takes for each thread to call the add function and the overhead due to the context switches but is inversely proportional to the number of operations. Since the total time stays the time but the number of operations increases, the average cost per operation will decrease.

QUESTION 2A.2B: How do we know what the “correct” cost is?
ANSWER 2A.2B: The correct cost is just the cost for to execute the add function. Since the average cost per operation depends on the cost of executing add, we can find the correct cost by finding the limit as the number of iterations goes to infinity. Even though average cost per operation also depends on the cost of creating threads, the overhead is consistent because the number of threads to create are consistent and thus the overhead cost per operation will approach zero as the limit is taken, leaving only the correct cost.

QUESTION 2A.2C: Why are the --yield runs so much slower?  Where is the extra time going?
ANSWER 2A.2C: The --yield runs are much slower because threads yield to the processor so others threads can use it. Threads have to wait their turns in a queue while the next thread in line executes. The extra time is going into yielding during every single call to the add function by every single thread.

QUESTION 2A.2D: Can we get valid timings if we are using --yield?  How, or why not?
ANSWER 2A.2D: We cannot get valid timings by using --yield because the time it takes to yield the processor for each thread is unknown. The time it takes for a thread to yield can take anywhere from 0ms to 32 ms (the default maximum delay). If the time it took for each thread to yield the processor to the next thread in the queue was consistent, and we knew that time, we could divide the timings produced with the --yield option by the number of threads times that time to get valid timings. This calculation would work because the extra time is coming from the yields.

QUESTION 2A.3A: Why do all of the options perform similarly for low numbers of threads?
ANSWER 2A.3A: The options differ only in their execution costs due to different implementations of exclusion mechanisms. This means that each option will perform similarly for a low number of threads because there would be less context switching and thus less overhead from the exclusion mechanisms. The options start peforming differently as the number of threads increase because each exclusion mechanism will have to be called by more threads which forces their performances to be more dependent on their mechanisms.

QUESTION 2A.3B: Why do the three protected operations slow down as the number of threads rises?
ANSWER 2A.3B: The operations slow down because the time it takes for them to execute is dependent on the number of threads. It takes some amount of time for a thread to check if it is its turn to execute. By adding n more threads, each thread has to check n more times for that amount of time. The unprotected operations stay fairly consistent because there are no checking mechanisms that are done and thus the only time added due to rising number of threads is the time it takes for the additional threads to execute the operations.

QUESTION 2A.3C: Why are spin-locks so expensive for large numbers of threads?
ANSWER 2A.3C: Spin-locks are implemented so that every thread consistently checks if it is its turn to execute. Even though it may not be a thread's turn to execute yet, it will still require the processor to just check if it is its turn. The thread that is supposed to execute then has to wait longer as the number of threads is increased.

------------------------
REFERENCES

WEBSITES

Multiple arguments to function called by pthread_create()?
http://stackoverflow.com/questions/1352749/multiple-arguments-to-function-called-by-pthread-create

Printing long int value in C
http://stackoverflow.com/questions/17779570/printing-long-int-value-in-c

PTHREAD_MUTEX_INITIALIZER vs pthread_mutex_init ( &mutex, param)
http://stackoverflow.com/questions/14320041/pthread-mutex-initializer-vs-pthread-mutex-init-mutex-param

GNU Built-in functions for atomic memory access
https://gcc.gnu.org/onlinedocs/gcc-4.4.3/gcc/Atomic-Builtins.html

Is my spin lock implementation correct and optimal?
http://stackoverflow.com/questions/1383363/is-my-spin-lock-implementation-correct-and-optimal

How to Use C's volatile Keyword
http://www.barrgroup.com/Embedded-Systems/How-To/C-Volatile-Keyword
