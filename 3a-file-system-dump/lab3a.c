#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/*------- Constants -----------------------*/
// Misc
static const int GROUP_DESCRIPTOR_SIZE = 32;

// File types
static const char FILE_TYPE = 'f';
static const char DIRECTORY_TYPE = 'd';
static const char SYMBOLIC_LINK_TYPE = 's';
static const char OTHER_TYPE = '?';

/*------- Structures ----------------------*/
struct superblock {
  uint16_t magic_number;
  uint32_t inode_count;
  uint16_t inode_size;
  uint32_t block_count;
  uint32_t block_size;
  uint32_t fragment_size;
  uint32_t blocks_per_group;
  uint32_t inodes_per_group;
  uint32_t fragments_per_group;
  uint32_t first_data_block;
  uint32_t revision_level;
};

struct group_descriptor {
  uint32_t total_block_count;
  uint16_t free_block_count;
  uint16_t free_inode_count;
  uint16_t allocated_inode_count;
  uint16_t directory_count;
  uint32_t start_of_inode_bitmap;
  uint32_t start_of_block_bitmap;
  uint32_t inode_table_start;
};

struct inode {
  char file_type;
  uint32_t inode_number;
  uint16_t mode;
  uint32_t owner;
  uint32_t group;
  uint16_t link_count;
  uint32_t create_time;
  uint32_t mod_time;
  uint32_t access_time;
  uint32_t file_size;
  uint32_t block_count;
  uint32_t block_pointers[15];
};

struct directory_entry {
  uint32_t parent_inode_number;
  uint32_t entry_number;
  uint16_t entry_length;
  char name_length;
  uint32_t inode_number;
  char* name;
};

struct indirect_block_entry {
  uint32_t indirect_block_number;
  uint32_t entry_number;
  uint32_t block_pointer_value;
};

/*------- Global Variables ----------------*/
// Used for storing values into structures
static uint16_t* ptr2;
static uint32_t* ptr4;

// Used for traversing
static char* traverse;
static char* entry_start;

/*------- Main ----------------------------*/
int main(int argc, char* argv[]) {

  if(argc != 2) {
    fprintf(stderr, "ERROR: number of arguments given should be 2.\n");
    exit(EXIT_FAILURE);
  }

  // For loop traversals
  int i;
  int j;
  int k;
  int l;
  int m;
  int n;

  /*--------- Open file system for reading --------*/
  int fd_disk = -1;
  if((fd_disk = open(argv[1], O_RDONLY)) == -1) {
    fprintf(stderr, "ERROR: could not open %s.\n", argv[1]);
    exit(EXIT_FAILURE);
  }

  /*--------- super.csv ---------------------------*/

  struct superblock fs_superblock;

  // Read superblock
  char read_superblock[1024];
  if(pread(fd_disk, read_superblock, 1024, 1024) != 1024) {  
    fprintf(stderr, "ERROR: could not read superblock from fs.\n");
    exit(EXIT_FAILURE);
  }

  // Revision level
  traverse = read_superblock + 76;
  ptr4 = (uint32_t *) traverse;
  fs_superblock.revision_level = *ptr4;
  
  // Magic number
  traverse = read_superblock + 56;
  ptr2 = (uint16_t *) traverse;
  fs_superblock.magic_number = *ptr2;

  // Inode size
  if(fs_superblock.revision_level == 0) // revision 0
    fs_superblock.inode_size = 128;
  else { // revision 1
    traverse = read_superblock + 88;
    ptr2 = (uint16_t *) traverse;
    fs_superblock.inode_size = *ptr2;
  }

  int inode_size = fs_superblock.inode_size;
  
  // Inode count
  traverse = read_superblock;
  ptr4 = (uint32_t *) traverse;
  fs_superblock.inode_count = *ptr4;

  // Block count
  traverse = read_superblock + 4;
  ptr4 = (uint32_t *) traverse;
  fs_superblock.block_count = *ptr4;

  // Block size
  traverse = read_superblock + 24;
  ptr4 = (uint32_t *) traverse;
  fs_superblock.block_size = 1024 << *ptr4;

  int block_size = fs_superblock.block_size;

  // Fragment size
  traverse = read_superblock + 28;
  ptr4 = (uint32_t *) traverse;
  if(*ptr4 >= 0) fs_superblock.fragment_size = 1024 << *ptr4;
  else fs_superblock.fragment_size = 1024 >> *ptr4;

  // Blocks per group
  traverse = read_superblock + 32;
  ptr4 = (uint32_t *) traverse;
  fs_superblock.blocks_per_group = *ptr4;

  // Inodes per group
  traverse = read_superblock + 40;
  ptr4 = (uint32_t *) traverse;
  fs_superblock.inodes_per_group = *ptr4;

  int inodes_per_group = fs_superblock.inodes_per_group;

  // Fragments per group
  traverse = read_superblock + 36;
  ptr4 = (uint32_t *) traverse;
  fs_superblock.fragments_per_group = *ptr4;

  // First data block
  traverse = read_superblock + 20;
  ptr4 = (uint32_t *) traverse;
  fs_superblock.first_data_block = *ptr4;
  
  /*--------- group.csv ---------------------------*/

  int num_groups = (fs_superblock.block_count + (fs_superblock.blocks_per_group - 1)) / fs_superblock.blocks_per_group;
  int group_descriptor_table_size = GROUP_DESCRIPTOR_SIZE * num_groups;
  
  // Read in group descriptor table
  char read_group_descriptor_table[group_descriptor_table_size];
  if(pread(fd_disk, read_group_descriptor_table, group_descriptor_table_size, (num_groups - 1) * block_size) != group_descriptor_table_size) {  
    fprintf(stderr, "ERROR: could not read group descriptor table from fs.\n");
    exit(EXIT_FAILURE);
  }

  // Fill in group descriptors
  struct group_descriptor group_descriptor_table[num_groups];
  for(i = 0; i < num_groups; i++) {

    // Number of contained blocks
    if(i == num_groups-1) // Last group
      group_descriptor_table[i].total_block_count = fs_superblock.block_count % fs_superblock.blocks_per_group;
    else
      group_descriptor_table[i].total_block_count = fs_superblock.blocks_per_group;

    // start of block bitmap
    traverse = read_group_descriptor_table + (i*GROUP_DESCRIPTOR_SIZE);
    ptr4 = (uint32_t *) traverse;
    group_descriptor_table[i].start_of_block_bitmap = *ptr4;
    
    // start of inode bitmap
    traverse += 4;
    ptr4 = (uint32_t *) traverse;
    group_descriptor_table[i].start_of_inode_bitmap = *ptr4;

    // Inode table starting block
    traverse += 4;
    ptr4 = (uint32_t *) traverse;
    group_descriptor_table[i].inode_table_start = *ptr4;
    
    // Number of free blocks
    traverse += 4;
    ptr2 = (uint16_t *) traverse;
    group_descriptor_table[i].free_block_count = *ptr2;

    // Number of free inodes
    traverse += 2;
    ptr2 = (uint16_t *) traverse;
    group_descriptor_table[i].free_inode_count = *ptr2;

    // Number of directories
    traverse += 2;
    ptr2 = (uint16_t *) traverse;
    group_descriptor_table[i].directory_count = *ptr2;

  }
  
  /*--------- bitmap.csv ---------------------------*/
  
  // Grab free block numbers
  int* free_blocks[num_groups];
  int free_blocks_count[num_groups];
  int total_blocks_past = 0;
  for(i = 0; i < num_groups; i++) {
    
    free_blocks_count[i] = 0;
    int start_of_bitmap = group_descriptor_table[i].start_of_block_bitmap * block_size;
    int total_block_count = group_descriptor_table[i].total_block_count;

    // Read in bitmap
    char read_bitmap[block_size];
    if(pread(fd_disk, read_bitmap, block_size, start_of_bitmap) != block_size) {  
      fprintf(stderr, "ERROR: could not read_group[%d] from fs.\n", i);
      exit(EXIT_FAILURE);
    }

    // Traverse bitmap
    traverse = read_bitmap;
    free_blocks[i] = (int *)malloc((free_blocks_count[i]+1) * sizeof(int));
    for(j = 0; j < block_size; j++) {

      for(k = 0; k < 8; k++) 
	if(((*traverse >> k) & 0x1) == 0) { // Kth bit of byte is free
	    free_blocks[i][free_blocks_count[i]++] = total_blocks_past + j*8 + k + 1;
	    free_blocks[i] = (int *)realloc(free_blocks[i], (free_blocks_count[i]+1) * sizeof(int));
	}
      traverse++;
    }
    
    total_blocks_past += total_block_count;
  }
  
  // Create lists to hold inode numbers
  int* free_inodes[num_groups];
  int free_inodes_count[num_groups];
  int* allocated_inodes[num_groups];
  int allocated_inodes_count[num_groups];
  for(i = 0; i < num_groups; i++) {

    free_inodes_count[i] = 0;
    allocated_inodes_count[i] = 0;

    // Read bitmap
    int start_of_bitmap = group_descriptor_table[i].start_of_inode_bitmap * block_size;
    char read_bitmap[block_size];
    if(pread(fd_disk, read_bitmap, block_size, start_of_bitmap) != block_size) {  
      fprintf(stderr, "ERROR: could not read_bitmap of group %d from fs.\n", i);
      exit(EXIT_FAILURE);
    }

    // Traverse bitmap
    traverse = read_bitmap;
    free_inodes[i] = (int *)malloc((free_inodes_count[i]+1) * sizeof(int));
    allocated_inodes[i] = (int *)malloc((allocated_inodes_count[i]+1) * sizeof(int));
    for(j = 0; j < block_size; j++) {   
      for(k = 0; k < 8; k++) {
	if(((*traverse >> k) & 0x1) == 0) {  // Kth bit of byte is free
	    free_inodes[i][free_inodes_count[i]++] = i*inodes_per_group + j*8 + k + 1;
	    free_inodes[i] = (int *)realloc(free_inodes[i], (free_inodes_count[i]+1) * sizeof(int));
	}

	else if(j < inodes_per_group/8) {
	    allocated_inodes[i][allocated_inodes_count[i]++] = i*inodes_per_group + j*8 + k + 1;
	    allocated_inodes[i] = (int *)realloc(allocated_inodes[i], (allocated_inodes_count[i]+1) * sizeof(int));
	}
      }
      
      traverse++;
    }
  }

  /*--------- inode.csv ---------------------------*/
  int inode_table_size = fs_superblock.inodes_per_group * inode_size;
  
  struct inode* inodes[num_groups];
  for(i = 0; i < num_groups; i++) {

    // Read in inode table
    int inode_table_start = group_descriptor_table[i].inode_table_start * block_size;
    char read_inode_table[inode_table_size];
    if(pread(fd_disk, read_inode_table, inode_table_size, inode_table_start) != inode_table_size) {  
      fprintf(stderr, "ERROR: could not read_inode_table of group %d from fs.\n", i);
      exit(EXIT_FAILURE);
    }

    int offset = 0;
    inodes[i] = (struct inode *) malloc(allocated_inodes_count[i] * sizeof(struct inode));
    for(j = 0; j < allocated_inodes_count[i]; j++) {

      if(j == 0);
      else offset += allocated_inodes[i][j] - allocated_inodes[i][j-1];

      // Inode number
      inodes[i][j].inode_number = allocated_inodes[i][j];

      // Set traverse
      traverse = read_inode_table + offset * inode_size;

      // Mode
      ptr2 = (uint16_t *) traverse;
      inodes[i][j].mode = *ptr2;
      
      // File type
      if(inodes[i][j].mode & 0x8000) inodes[i][j].file_type = FILE_TYPE;
      else if(inodes[i][j].mode & 0x4000) inodes[i][j].file_type = DIRECTORY_TYPE;
      else if(inodes[i][j].mode & 0xA000) inodes[i][j].file_type = SYMBOLIC_LINK_TYPE;
      else inodes[i][j].file_type = OTHER_TYPE;

      // Owner (user id)
      traverse += 2;
      ptr2 = (uint16_t *) traverse;
      inodes[i][j].owner = *ptr2;

      // File size
      traverse += 2;
      ptr4 = (uint32_t *) traverse;
      inodes[i][j].file_size = *ptr4;

      // Access time
      traverse += 4;
      ptr4 = (uint32_t *) traverse;
      inodes[i][j].access_time = *ptr4;

      // Create time
      traverse += 4;
      ptr4 = (uint32_t *) traverse;
      inodes[i][j].create_time = *ptr4;

      // Mod time
      traverse += 4;
      ptr4 = (uint32_t *) traverse;
      inodes[i][j].mod_time = *ptr4;
      
      // Group id
      traverse += 8;
      ptr2 = (uint16_t *) traverse;
      inodes[i][j].group = *ptr2;

      // Links count
      traverse += 2;
      ptr2 = (uint16_t *) traverse;
      inodes[i][j].link_count = *ptr2;

      // Block count
      traverse += 2;
      ptr4 = (uint32_t *) traverse;
      inodes[i][j].block_count = 512*(*ptr4)/block_size;

      // Block pointers
      traverse += 12;
      for(k = 0; k < 15; k++) {
	ptr4 = (uint32_t *) traverse;
	inodes[i][j].block_pointers[k] = *ptr4;
	traverse += 4;
      }
    }
  }
  
  /*--------- directory.csv ---------------------------*/

  struct directory_entry* directory_entries[num_groups];
  int directory_entries_count[num_groups];
  for(i = 0; i < num_groups; i++) {

    directory_entries_count[i] = 0;

    directory_entries[i] = malloc((directory_entries_count[i]+1) * sizeof(struct directory_entry));
    for(j = 0; j < allocated_inodes_count[i]; j++) {
      
      if(inodes[i][j].file_type != DIRECTORY_TYPE)
	continue;

      int entry_number = 0;
      int block_count = inodes[i][j].block_count;

      for(k = 0; k < block_count; k++) {
	
	if(k == 12) { // indirect

	  int indirect_block_start = inodes[i][j].block_pointers[k] * block_size;
	  if(indirect_block_start == 0)
	     continue;

	  // Read in indirect block array locations
	  char read_indirect_block[block_size];
	  if(pread(fd_disk, read_indirect_block, block_size, indirect_block_start) != block_size) {  
	    fprintf(stderr, "ERROR: could not read indirect block array\n");
	    exit(EXIT_FAILURE);
	  }

	  // Store indirect block array locations
	  int *indirect_block = malloc((block_size/4)*sizeof(int));
	  traverse = read_indirect_block;
	  for(l = 0; l < block_size/4; l++) {
	    ptr4 = (uint32_t *) traverse;
	    indirect_block[l] = *ptr4;
	    traverse += 4;
	  }

	  // Traverse each block location in the indirect block array
	  for(l = 0; l < block_size/4; l++) {
	    int traversed_bytes = 0;
	    int directory_entries_start = indirect_block[l] * block_size;

	    if(directory_entries_start == 0)
	      continue;
	    
	    char read_directory_entries[block_size];
	    if(pread(fd_disk, read_directory_entries, block_size, directory_entries_start) != block_size) {  
	      fprintf(stderr, "ERROR: could not read directory entry %d of inode %d of group %d.\n", k, inodes[i][j].inode_number, i);
	      exit(EXIT_FAILURE);
	    }
	    
	    traverse = read_directory_entries;
	    while(1) {
	      
	      entry_start = traverse;
	      
	      // Parent inode number
	      directory_entries[i][directory_entries_count[i]].parent_inode_number = inodes[i][j].inode_number;
	      
	      // Entry number
	      directory_entries[i][directory_entries_count[i]].entry_number = entry_number++;
	      
	      // Inode number
	      ptr4 = (uint32_t *) traverse;
	      directory_entries[i][directory_entries_count[i]].inode_number = *ptr4;
	      
	      // Entry length
	      traverse += 4;
	      ptr2 = (uint16_t *) traverse;
	      directory_entries[i][directory_entries_count[i]].entry_length = *ptr2;
	      
	      // Name length
	      traverse += 2;
	      directory_entries[i][directory_entries_count[i]].name_length = *traverse;
	      int name_length = (int) directory_entries[i][directory_entries_count[i]].name_length;

	      if(name_length == 0)
	      break;

	      // Name
	      traverse += 2;
	      directory_entries[i][directory_entries_count[i]].name = malloc(name_length * sizeof(char));
	      memcpy(directory_entries[i][directory_entries_count[i]].name, traverse, name_length);
	      directory_entries[i][directory_entries_count[i]].name[name_length] = '\0';
	      
	      if((traversed_bytes += directory_entries[i][directory_entries_count[i]].entry_length) >= block_size) {
		directory_entries[i] = realloc(directory_entries[i], (directory_entries_count[i]+2) * sizeof(struct directory_entry));
		directory_entries_count[i]++;
		break;
	      }
	      
	      traverse = entry_start + directory_entries[i][directory_entries_count[i]].entry_length;

	      if(directory_entries[i][directory_entries_count[i]].inode_number == 0)
		continue;
	      
	      directory_entries[i] = realloc(directory_entries[i], (directory_entries_count[i]+2) * sizeof(struct directory_entry));
	      directory_entries_count[i]++;
	    }
	  }
	}
	
	if(k < 12) { // direct

	  int traversed_bytes = 0;
	  int directory_entries_start = inodes[i][j].block_pointers[k] * block_size;

	  char read_directory_entries[block_size];
	  if(pread(fd_disk, read_directory_entries, block_size, directory_entries_start) != block_size) {  
	    fprintf(stderr, "ERROR: could not read directory entry %d of inode %d of group %d.\n", k, inodes[i][j].inode_number, i);
	    exit(EXIT_FAILURE);
	  }
	  
	  traverse = read_directory_entries;
	  while(1) {
	    	    
	    entry_start = traverse;
	    
	    // Parent inode number
	    directory_entries[i][directory_entries_count[i]].parent_inode_number = inodes[i][j].inode_number;
	    
	    // Entry number
	    directory_entries[i][directory_entries_count[i]].entry_number = entry_number++;
	    
	    // Inode number
	    ptr4 = (uint32_t *) traverse;
	    directory_entries[i][directory_entries_count[i]].inode_number = *ptr4;

	    // Entry length
	    traverse += 4;
	    ptr2 = (uint16_t *) traverse;
	    directory_entries[i][directory_entries_count[i]].entry_length = *ptr2;
	    
	    // Name length
	    traverse += 2;
	    directory_entries[i][directory_entries_count[i]].name_length = *traverse;
	    int name_length = (int) directory_entries[i][directory_entries_count[i]].name_length;

	    if(name_length == 0)
	      break;
	    
	    // Name
	    traverse += 2;
	    directory_entries[i][directory_entries_count[i]].name = malloc((name_length+1) * sizeof(char));
	    memcpy(directory_entries[i][directory_entries_count[i]].name, traverse, name_length);
	    directory_entries[i][directory_entries_count[i]].name[name_length] = '\0';
	    
	    if((traversed_bytes += directory_entries[i][directory_entries_count[i]].entry_length) >= block_size) {
	      directory_entries[i] = realloc(directory_entries[i], (directory_entries_count[i]+2) * sizeof(struct directory_entry));
	      directory_entries_count[i]++;
	      break;
	    }
	    
	    traverse = entry_start + directory_entries[i][directory_entries_count[i]].entry_length;

	    if(directory_entries[i][directory_entries_count[i]].inode_number == 0) {
	      free(directory_entries[i][directory_entries_count[i]].name);
	      continue;
	    }

	    directory_entries[i] = realloc(directory_entries[i], (directory_entries_count[i]+2) * sizeof(struct directory_entry));
	    directory_entries_count[i]++;
	  }
	}
      }
    }
  }
  
  /*--------- indirect.csv ---------------------------*/
  
  int indirect_block_entries_count[num_groups];
  struct indirect_block_entry* indirect_block_entries[num_groups];
  for(i = 0; i < num_groups; i++) {
    
    indirect_block_entries_count[i] = 0;
    
    indirect_block_entries[i] = malloc((indirect_block_entries_count[i]+1) * sizeof(struct indirect_block_entry));
    for(j = 0; j < allocated_inodes_count[i]; j++) {
     
      int block_count = inodes[i][j].block_count;
      
      for(k = 12; k < block_count; k++) {

	int entry_number = 0;
	
	if(k == 12) { // indirect
	  
	  int indirect_block_start = inodes[i][j].block_pointers[k] * block_size;
	  if(indirect_block_start == 0)
	    continue;
	  
	  // Read in direct locations
	  char read_indirect_block[block_size];
	  if(pread(fd_disk, read_indirect_block, block_size, indirect_block_start) != block_size) {  
	    fprintf(stderr, "ERROR: could not read indirect block array\n");
	    exit(EXIT_FAILURE);
	  }
	  
	  // Store direct block values
	  traverse = read_indirect_block;
	  entry_number = 0;
	  for(l = 0; l < block_size/4; l++) {
	    
	    // Entry number
	    indirect_block_entries[i][indirect_block_entries_count[i]].entry_number = entry_number++;
	    
	    // Block pointer value
	    ptr4 = (uint32_t *) traverse;
	    traverse += 4;
	    if((indirect_block_entries[i][indirect_block_entries_count[i]].block_pointer_value = *ptr4) == 0)
	      continue;
	  
	    // Containing block number
	    indirect_block_entries[i][indirect_block_entries_count[i]].indirect_block_number = inodes[i][j].block_pointers[k];
	    
	    indirect_block_entries[i] = realloc(indirect_block_entries[i], (indirect_block_entries_count[i]+2) * sizeof(struct indirect_block_entry));
	    indirect_block_entries_count[i]++;
	  }
	}

	if(k == 13) { // doubly
	  
	  int doubly_block_start = inodes[i][j].block_pointers[k] * block_size;
	  if(doubly_block_start == 0)
	    continue;
	  
	  // Read in indirect locations
	  char read_doubly_block[block_size];
	  if(pread(fd_disk, read_doubly_block, block_size, doubly_block_start) != block_size) {  
	    fprintf(stderr, "ERROR: could not read doubly block array\n");
	    exit(EXIT_FAILURE);
	  }

	  // Print indirect pointers values
	  traverse = read_doubly_block;
	  entry_number = 0;
	  for(l = 0; l < block_size/4; l++) {
	    
	    // Entry number
	    indirect_block_entries[i][indirect_block_entries_count[i]].entry_number = entry_number++;
	      
	    // Block pointer value
	    ptr4 = (uint32_t *) traverse;
	    traverse += 4;
	    if((indirect_block_entries[i][indirect_block_entries_count[i]].block_pointer_value = *ptr4) == 0)
	      continue;
	    
	    // Containing block number
	    ptr4 = (uint32_t *) entry_start;
	    indirect_block_entries[i][indirect_block_entries_count[i]].indirect_block_number = inodes[i][j].block_pointers[k];
	    
	    indirect_block_entries[i] = realloc(indirect_block_entries[i], (indirect_block_entries_count[i]+2) * sizeof(struct indirect_block_entry));
	    indirect_block_entries_count[i]++;
	  }

	  // Loop through every indirect location to get direct blocks
	  entry_start = read_doubly_block;
	  for(l = 0; l < block_size/4; l++) {
	    
	    ptr4 = (uint32_t *) entry_start;
	    int indirect_block_start = *ptr4 * block_size;
	    
	    // Read in direct locations
	    char read_indirect_block[block_size];
	    if(pread(fd_disk, read_indirect_block, block_size, indirect_block_start) != block_size) {  
	      fprintf(stderr, "ERROR: could not read indirect block array\n");
	      exit(EXIT_FAILURE);
	    }
	    
	    // Store direct block values
	    traverse = read_indirect_block;
	    entry_number = 0;
	    for(m = 0; m < block_size/4; m++) {
	      
	      // Entry number
	      indirect_block_entries[i][indirect_block_entries_count[i]].entry_number = entry_number++;
	      
	      // Block pointer value
	      ptr4 = (uint32_t *) traverse;
	      traverse += 4;
	      if((indirect_block_entries[i][indirect_block_entries_count[i]].block_pointer_value = *ptr4) == 0)
		continue;
	      
	      // Containing block number
	      ptr4 = (uint32_t *) entry_start;
	      indirect_block_entries[i][indirect_block_entries_count[i]].indirect_block_number = *ptr4;
	      
	      indirect_block_entries[i] = realloc(indirect_block_entries[i], (indirect_block_entries_count[i]+2) * sizeof(struct indirect_block_entry));
	      indirect_block_entries_count[i]++;
	    }

	    entry_start += 4;
	  }

	  if(k == 14) { // trebly

	    int trebly_block_start = inodes[i][j].block_pointers[k];
	    if(trebly_block_start == 0)
	      continue;

	    // Read in trebly
	    char read_trebly_block[block_size];
	    if(pread(fd_disk, read_trebly_block, block_size, trebly_block_start) != block_size) {  
	      fprintf(stderr, "ERROR: could not read trebly block array\n");
	      exit(EXIT_FAILURE);
	    }

	    // Print trebly pointer values
	    traverse = read_trebly_block;
	    entry_number = 0;
	    for(l = 0; l < block_size/4; l++) {
	      
	      // Entry number
	      indirect_block_entries[i][indirect_block_entries_count[i]].entry_number = entry_number++;
	      
	      // Block pointer value
	      ptr4 = (uint32_t *) traverse;
	      traverse += 4;
	      if((indirect_block_entries[i][indirect_block_entries_count[i]].block_pointer_value = *ptr4) == 0)
		continue;
	      
	      // Containing block number
	      ptr4 = (uint32_t *) entry_start;
	      indirect_block_entries[i][indirect_block_entries_count[i]].indirect_block_number = inodes[i][j].block_pointers[k];
	      
	      indirect_block_entries[i] = realloc(indirect_block_entries[i], (indirect_block_entries_count[i]+2) * sizeof(struct indirect_block_entry));
	      indirect_block_entries_count[i]++;
	    }
	    
	    // Loop through every doubly location to get indirect locations to get direct blocks
	    char *doubly_start = read_trebly_block;
	    for(l = 0; l < block_size/4; l++) {

	      ptr4 = (uint32_t *) doubly_start;
	      int doubly_contained = *ptr4;
	      int doubly_block_start = *ptr4 * block_size;
	      if(doubly_block_start == 0)
		continue;
	      
	      // Read in indirect locations
	      char read_doubly_block[block_size];
	      if(pread(fd_disk, read_doubly_block, block_size, doubly_block_start) != block_size) {  
		fprintf(stderr, "ERROR: could not read doubly block array\n");
		exit(EXIT_FAILURE);
	      }

	      // Print indirect pointers values
	      traverse = read_doubly_block;
	      entry_number = 0;
	      for(l = 0; l < block_size/4; l++) {
		
		// Entry number
		indirect_block_entries[i][indirect_block_entries_count[i]].entry_number = entry_number++;
		
		// Block pointer value
		ptr4 = (uint32_t *) traverse;
		traverse += 4;
		if((indirect_block_entries[i][indirect_block_entries_count[i]].block_pointer_value = *ptr4) == 0)
		  continue;
		
		// Containing block number
		ptr4 = (uint32_t *) entry_start;
		indirect_block_entries[i][indirect_block_entries_count[i]].indirect_block_number = doubly_contained ;
		
		indirect_block_entries[i] = realloc(indirect_block_entries[i], (indirect_block_entries_count[i]+2) * sizeof(struct indirect_block_entry));
		indirect_block_entries_count[i]++;
	      }
	      
	      // Loop through every indirect location to get direct blocks
	      entry_start = read_doubly_block;
	      for(m = 0; m < block_size/4; m++) {
		
		int entry_number = 0;
		
		ptr4 = (uint32_t *) entry_start;
		int indirect_block_start = *ptr4 * block_size;
		
		// Read in direct locations
		char read_indirect_block[block_size];
		if(pread(fd_disk, read_indirect_block, block_size, indirect_block_start) != block_size) {  
		  fprintf(stderr, "ERROR: could not read indirect block array\n");
		exit(EXIT_FAILURE);
		}
		
		// Store direct block values
		traverse = read_indirect_block;
		for(n = 0; n < block_size/4; n++) {
	      
		  // Entry number
		  indirect_block_entries[i][indirect_block_entries_count[i]].entry_number = entry_number++;
		  
		  // Block pointer value
		  ptr4 = (uint32_t *) traverse;
		  traverse += 4;
		  if((indirect_block_entries[i][indirect_block_entries_count[i]].block_pointer_value = *ptr4) == 0)
		    continue;
		  
		  // Containing block number
		  ptr4 = (uint32_t *) entry_start;
		  indirect_block_entries[i][indirect_block_entries_count[i]].indirect_block_number = *ptr4;
		  
		  indirect_block_entries[i] = realloc(indirect_block_entries[i], (indirect_block_entries_count[i]+2) * sizeof(struct indirect_block_entry));
		  indirect_block_entries_count[i]++;
		}
		
		entry_start += 4;
	      }
	      
	      doubly_start += 4;
	    }
	  }
	}
      }
    }
  }
  
  /*--------- Write to files -------------------------*/
  FILE* super_csv = fopen("super.csv", "w");
  FILE* group_csv = fopen("group.csv", "w");
  FILE* bitmap_csv = fopen("bitmap.csv", "w");
  FILE* inode_csv = fopen("inode.csv", "w");
  FILE* directory_csv = fopen("directory.csv", "w");
  FILE* indirect_csv = fopen("indirect.csv", "w");

  // super.csv
  fprintf(super_csv, "%x,%d,%d,%d,%d,%d,%d,%d,%d\n"
	  , fs_superblock.magic_number
	  , fs_superblock.inode_count
	  , fs_superblock.block_count
	  , fs_superblock.block_size
	  , fs_superblock.fragment_size
	  , fs_superblock.blocks_per_group
	  , fs_superblock.inodes_per_group
	  , fs_superblock.fragments_per_group
	  , fs_superblock.first_data_block);

  // group.csv
  for(i = 0; i < num_groups; i++)
    fprintf(group_csv, "%d,%d,%d,%d,%x,%x,%x\n"
	    , group_descriptor_table[i].total_block_count
	    , group_descriptor_table[i].free_block_count
	    , group_descriptor_table[i].free_inode_count
	    , group_descriptor_table[i].directory_count
	    , group_descriptor_table[i].start_of_inode_bitmap
	    , group_descriptor_table[i].start_of_block_bitmap
	    , group_descriptor_table[i].inode_table_start);
  
  // bitmap.csv
  for(i = 0; i < num_groups; i++) {
    
    for(j = 0; j < free_blocks_count[i]; j++)
      fprintf(bitmap_csv, "%x,%d\n"
	      , group_descriptor_table[i].start_of_block_bitmap
	      , free_blocks[i][j]);
	      
    for(j = 0; j < free_inodes_count[i]; j++)
      fprintf(bitmap_csv, "%x,%d\n"
	      , group_descriptor_table[i].start_of_inode_bitmap
	      , free_inodes[i][j]);
  }
  
  // inode.csv
  for(i = 0; i < num_groups; i++)
    for(j = 0; j < allocated_inodes_count[i]; j++) {
      fprintf(inode_csv, "%d,%c,%o,%d,%d,%d,%x,%x,%x,%d,%d"
	      , inodes[i][j].inode_number
	      , inodes[i][j].file_type
	      , inodes[i][j].mode
	      , inodes[i][j].owner
	      , inodes[i][j].group
	      , inodes[i][j].link_count
	      , inodes[i][j].create_time
	      , inodes[i][j].mod_time
	      , inodes[i][j].access_time
	      , inodes[i][j].file_size
	      , inodes[i][j].block_count);
      
      for(k = 0; k < 15; k++)
	fprintf(inode_csv, ",%x", inodes[i][j].block_pointers[k]);
      
      fprintf(inode_csv, "\n");
    }
  
  // directory.csv
  for(i = 0; i < num_groups; i++)
    for(j = 0; j < directory_entries_count[i]; j++)
      fprintf(directory_csv, "%d,%d,%d,%d,%d,\"%s\"\n"
	      , directory_entries[i][j].parent_inode_number
	      , directory_entries[i][j].entry_number
	      , directory_entries[i][j].entry_length
	      , directory_entries[i][j].name_length
	      , directory_entries[i][j].inode_number
	      , directory_entries[i][j].name);
  
  // indirect.csv
  for(i = 0; i < num_groups; i++)
    for(j = 0; j < indirect_block_entries_count[i]; j++)
      fprintf(indirect_csv, "%x,%d,%x\n"
	      , indirect_block_entries[i][j].indirect_block_number
	      , indirect_block_entries[i][j].entry_number
	      , indirect_block_entries[i][j].block_pointer_value);

  fclose(super_csv);
  fclose(group_csv);
  fclose(bitmap_csv);
  fclose(inode_csv);
  fclose(directory_csv);
  fclose(indirect_csv);

  /*---------------- Free Memory ---------------------*/
  
  for(i = 0; i < num_groups; i++) {
    free(free_blocks[i]);
    free(free_inodes[i]);
    free(allocated_inodes[i]);
    free(inodes[i]);
    for(j = 0; j <= directory_entries_count[i]; j++)
      free(directory_entries[i][j].name);
    free(directory_entries[i]);
    free(indirect_block_entries[i]);
  }

  exit(EXIT_SUCCESS);
}
