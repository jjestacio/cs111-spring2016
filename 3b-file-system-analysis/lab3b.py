###################################################
######### Definitions #############################
###################################################

##########
# Output #
##########

lab3b_output = open('lab3b_check.txt', 'w')

###########
# Classes #
###########

class Inode:
    def __init__(self, inode_number, num_links, num_blocks, block_pointers):
        self.inode_number = inode_number
        self.referenced_by = [] #[directory_inode_number, entry_number]
        self.num_links = num_links
        self.num_blocks = num_blocks
        self.block_pointers = block_pointers

class Block:
    def __init__(self, block_number):
        self.block_number = block_number
        self.referenced_by = [] #[inode_number, indirect_block_number, entry_number]
#########
# Lists #
#########

block_bitmap_blocks = [];
inode_bitmap_blocks = [];
free_blocks = [];
free_inodes = [];

indirect_pointers = []; #block_number
doubly_indirect_pointers = []; #block_number
trebly_indirect_pointers = []; #block_number

################
# Dictionaries #
################

superblock = {'num_total_inodes': None, 'num_total_blocks': None, 'block_size': None, 'blocks_per_group': None, 'inodes_per_group': None}
allocated_inodes = {} #{inode_number, object}
allocated_blocks = {} #{block_number, object}
indirect_table = {} #{containing_block_number-entry_number, pointer_value}
directory_table = {} #{parent_inode_number-entry_number, [inode_number, entry_name]}
parent_child_dictionary = {}#{child_inode_number, parent_inode_number}

###################################################
######### Functions ###############################
###################################################

##########
# Errors #
##########

def INVALID_BLOCK_ERROR(entry_number, block_number, inode_number, indirect_block_number):
    
    # if referenced by a direct block
    if indirect_block_number == 0:
        lab3b_output.write('INVALID BLOCK < %s > IN INODE < %s > ENTRY < %s >\n' % (block_number, inode_number, entry_number))

    # if referenced by an indirect block
    else:
        output_file.write('INVALID BLOCK < %s > IN INODE < %s > INDIRECT BLOCK < %s > ENTRY < %s >\n' % (block_number, inode_number, indirect_block_number, entry_number))
                              
def UNALLOCATED_BLOCK_ERROR(block):
    lab3b_output.write('UNALLOCATED BLOCK < %s > REFERENCED BY ' % block.block_number)

    # Write all references that block is referenced by
    block.referenced_by.sort()
    for i in range(0, len(block.referenced_by)):
    
        # block is not in stored in an indirect block
        if block.referenced_by[i][1] == 0:
            lab3b_output.write('INODE < %s > ENTRY < %s >' % (block.referenced_by[i][0], block.referenced_by[i][2]))

        # block is stored in an indirect block
        else:
            lab3b_output.write('INODE < %s > INDIRECT BLOCK < %s > ENTRY < %s >' % (block.referenced_by[i][0], block.referenced_by[i][1], block.referenced_by[i][2]))

        lab3b_output.write('\n')
            
def MULTIPLE_REFERENCE_ERROR(block):
    lab3b_output.write('MULTIPLY REFERENCED BLOCK < %s > BY' % block.block_number)

    # Write all references block is referenced by
    for reference in block.referenced_by:
        
        #reference is direct
        if reference[1] == 0:
            lab3b_output.write('INODE < %s > ENTRY < %s >' % (reference[0], reference[2]))

        #reference is indirect
        else:
            lab3b_output.write('INODE < %s > INDIRECT_BLOCK < %s > ENTRY < %s >' % (reference[0], reference[1], reference[2]))

    lab3b_output.write('\n')

def UNALLOCATED_INODE_ERROR(inode_number, directory, entry_number):
    lab3b_output.write('UNALLOCATED INODE < %s > REFERENCED BY DIRECTORY < %s > ENTRY < %s >\n' % (inode_number, directory, entry_number))
    
def MISSING_INODE_ERROR(inode_number):
    lab3b_output.write('MISSING INODE < %s > ' % inode_number)

    # Calculate which inode bitmap the inode would belong in
    inode_bitmap_blocks.sort()
    for group_number in range(0, len(inode_bitmap_blocks)):

        if inode_number >= superblock['num_total_inodes']/(len(inode_bitmap_blocks)-group_number):
            continue
        else:
            lab3b_output.write('SHOULD BE IN FREE LIST < %s >' %  inode_bitmap_blocks[group_number])
            break

    lab3b_output.write('\n')

def INCORRECT_LINK_COUNT_ERROR(inode_number, incorrect_link_count, correct_link_count):
    lab3b_output.write('LINKCOUNT < %s > IS < %s > SHOULD BE < %s >\n' % (inode_number, incorrect_link_count, correct_link_count))

def INCORRECT_ENTRY_ERROR(incorrect_inode_number, entry_name, directory_inode_number):
    lab3b_output.write('INCORRECT ENTRY IN < %s > NAME < %s > LINK TO < %s > SHOULD BE < %s >\n' % (directory_inode_number, entry_name, incorrect_inode_number, directory_inode_number))

##########
# Checks #
##########

def check_directories():

    for entry in directory_table:
        current_entry = entry.split('-')
        parent_inode = int(current_entry[0])
        entry_number = int(current_entry[1])
        inode_number = directory_table[entry][0]
        entry_name = directory_table[entry][1]

        # check '.' directories
        if entry_name == '.':
            if inode_number != parent_inode:
                INCORRECT_ENTRY_ERROR(inode_number, entry_name, parent_inode)
        # check '..' directories
        elif entry_name == '..': 
            if parent_inode == 2: #root directory
                if inode_number != parent_inode:
                    INCORRECT_ENTRY_ERROR(inode_number, entry_name, parent_inode)
            else: # all other directories
                if inode_number != parent_child_dictionary[parent_inode]:
                    INCORRECT_ENTRY_ERROR(inode_number, entry_name, parent_child_dictionary[parent_inode])

def check_indirect_pointers():

    for entry in indirect_table:
        current_entry = entry.split('-')
        containing_block_number = int(current_entry[0])
        containing_indirect_pointer = int(current_entry[0])
        entry_number = int(current_entry[1])
        block_number = indirect_table[entry]

        # check if pointed to by another indirect pointer
        while containing_block_number not in allocated_blocks:
            for other_entry in indirect_table:
                current_other_entry = other_entry.split('-')
                if containing_block_number ==  indirect_table[other_entry]:
                    containing_block_number = int(current_other_entry[0])
                    if containing_indirect_pointer == int(current_entry[0]): #register containing indirect pointer for the first iteration only
                        containing_indirect_pointer = containing_block_number
                    
        # get inode_number
        inode_number = allocated_blocks[containing_block_number].referenced_by[0][1]

        # invalid block number
        if block_number == 0 or block_number >= superblock['num_total_blocks']:
            INVALID_BLOCK_ERROR(entry_number, block_number, inode_number, indirect_block_number)

        # valid block number
        else:
            if block_number not in allocated_blocks:
                allocated_blocks[block_number] = Block(block_number)
            allocated_blocks[block_number].referenced_by.append([inode_number, containing_indirect_pointer, entry_number])

def allocate_blocks():
    
    for inode_number in allocated_inodes:
        num_blocks = allocated_inodes[inode_number].num_blocks
        for i in range(0, 15):

            # allocate direct blocks
            if i <= 11 and i < num_blocks:
                block_number = allocated_inodes[inode_number].block_pointers[i]

                # block is invalid
                if block_number == 0 or block_number >= superblock['num_total_blocks']:
                    INVALID_BLOCK_ERROR(i, block_number, inode_number, 0)

                # block is valid
                else:
                    if block_number not in allocated_blocks:
                        allocated_blocks[block_number] = Block(block_number)
                    allocated_blocks[block_number].referenced_by.append([inode_number, 0, i])

            # allocate single_indirect_block pointers
            if i == 12 and i < num_blocks:
                block_number = allocated_inodes[inode_number].block_pointers[i]

                # block is invalid
                if block_number >= superblock['num_total_blocks']:
                    INVALID_BLOCK_ERROR(i, block_number, inode_number, 0)

                # block is valid
                else:
                    if block_number not in allocated_blocks:
                        allocated_blocks[block_number] = Block(block_number)
                        #indirect_pointers.append(block_number)
                    allocated_blocks[block_number].referenced_by.append([inode_number, 0, i])

            # allocate doubly_indirect_block pointers
            if i == 13 and num_blocks - 11 - 256 > 0:
                block_number = allocated_inodes[inode_number].block_pointers[i]

                # block is invalid
                if block_number == 0 or block_number >= superblock['num_total_blocks']: #block is invalid
                    INVALID_BLOCK_ERROR(i, block_number, inode_number, 0)

                # block is valid
                else:
                    if block_number not in allocated_blocks:
                        allocated_blocks[block_number] = Block(block_number)
                        #doubly_indirect_pointers.append(block_number)
                    allocated_blocks[block_number].referenced_by.append([inode_number, 0, i])

            # allocate trebly_indirect_block pointers
            if i == 14 and num_blocks - 11 - 256 - 256*256 > 0:
                block_number = allocated_inodes[inode_number].block_pointers[i]

                # block is invalid
                if block_number == 0 or block_number >= superblock['num_total_blocks']:
                    INVALID_BLOCK_ERROR(i, block_number, inode_number, 0)

                # block is valid
                else:
                    if block_number not in allocated_blocks:
                        allocated_blocks[block_number] = Block(block_number)
                        #trebly_indirect_pointer.append(block_number)
                    allocated_blocks[block_number].referenced_by.append([inode_number, 0, i])
                
def check_unallocated_blocks():
    for block_number in allocated_blocks:
        if block_number in free_blocks:
            UNALLOCATED_BLOCK_ERROR(allocated_blocks[block_number])

def check_duplicately_allocated_blocks():
    for block_number in allocated_blocks:
        if len(allocated_blocks[block_number].referenced_by) > 1:
            MULTIPLE_REFERENCE_ERROR(allocated_blocks[block_number])

def check_missing_inodes():

    # store referenced inodes 
    directory = []
    for entry in directory_table:
        directory.append(directory_table[entry][0])
    
    for inode_number in range(11, superblock['num_total_inodes']):
        if inode_number not in free_inodes and inode_number not in directory:
            MISSING_INODE_ERROR(inode_number)

def check_link_counts():
    for inode_number in allocated_inodes:
        link_count = len(allocated_inodes[inode_number].referenced_by)
        if link_count != allocated_inodes[inode_number].num_links:
            INCORRECT_LINK_COUNT_ERROR(inode_number, allocated_inodes[inode_number].num_links, link_count)

###################################################
######### Main ####################################
###################################################

def main():

    ###################################
    # Grab information from csv files #
    ###################################
    
    # super.csv
    with open('super.csv') as input_file:
        for line in input_file:
            superblock_info = line.split(',')
            
    superblock['num_total_inodes'] = int(superblock_info[1])
    superblock['num_total_blocks'] = int(superblock_info[2])
    superblock['block_size'] = int(superblock_info[3])
    superblock['blocks_per_group'] = int(superblock_info[5])
    superblock['inodes_per_group'] = int(superblock_info[6])
            
    # group.csv
    group_descriptor_table = []
    with open('group.csv') as input_file:
        for line in input_file:
            group_descriptor_table.append((line.split(',')))

    for group in group_descriptor_table:
        block_bitmap_blocks.append(int(group[5]))
        inode_bitmap_blocks.append(int(group[4]))

    # bitmap.csv
    with open('bitmap.csv') as input_file:
        for line in input_file:
            current = line.rstrip().split(',')
            if int(current[0]) in block_bitmap_blocks:
                free_blocks.append(int(current[1]))
            elif int(current[0]) in inode_bitmap_blocks:
                free_inodes.append(int(current[1]))

    # indirect.csv
    with open('indirect.csv') as input_file:
        for line in input_file:
            current = line.rstrip().split(',')
            indirect_table['%s-%s' % (int(current[0], 16), int(current[1]))] = int(current[2], 16)

    # inode.csv
    with open('inode.csv') as input_file:
        for line in input_file:
            current = line.rstrip().split(',')
            
            block_pointers = [];
            for i in range(11,26):
                block_pointers.append(int(current[i], 16))

            inode_number = int(current[0])
            num_links = int(current[5])
            num_blocks = int(current[10])
            allocated_inodes[inode_number] = Inode(inode_number, num_links, num_blocks, block_pointers)
            

    # directory.csv
    with open('directory.csv') as input_file:
        for line in input_file:
            current = line.split(',', 5)
            if len(current) == 6:
                entry_name = current[5].strip('\n').replace("\"", "")
                inode_number = int(current[4])
                parent_inode = int(current[0])
                entry_number = int(current[1])
                directory_table['%s-%s' % (parent_inode, entry_number)] = [inode_number, entry_name]
       
                # check if inode is unallocated
                if inode_number in allocated_inodes:
                    allocated_inodes[inode_number].referenced_by.append([parent_inode, entry_number])
                else:
                    UNALLOCATED_INODE_ERROR(inode_number, parent_inode, entry_number)

    # parent_child_dictionary
    for entry in directory_table:
        current_entry = entry.split('-')
        entry_number = int(current_entry[1])
        parent_inode = int(current_entry[0])
        child_inode = directory_table[entry][0]
        parent_child_dictionary[child_inode] = parent_inode
                    
    #######################################
    # Check for the rest of the errors ####
    #######################################

    # creates and checks allocated blocks
    allocate_blocks()

    # checks indirect pointers and allocates blocks from
    check_indirect_pointers()

    # check unallocated blocks
    check_unallocated_blocks()

    # check for duplicately allocated blocks
    check_duplicately_allocated_blocks()

    # check all other directories
    check_directories()

    # check missing inodes
    check_missing_inodes()

    # check link counts
    check_link_counts()

    #########
    # Tests #
    #########
    
    """
    with open('allocated_blocks.txt', 'w'): pass
    with open('allocated_blocks.txt', 'a') as output_file:
        for block_number in allocated_blocks:
            print >>output_file, block_number

    with open('allocated_inodes.txt', 'w'): pass
    with open('allocated_inodes.txt', 'a') as output_file:
        for inode_number in allocated_inodes:
            print >>output_file, inode_number, allocated_inodes[inode_number].referenced_by

    with open('indirect_pointers.txt', 'w'): pass
    with open('indirect_pointers.txt', 'a') as output_file:
        for block_number in indirect_pointers:
            print >>output_file, block_number

    with open('doubly_indirect_pointers.txt', 'w'): pass
    with open('doubly_indirect_pointers.txt', 'a') as output_file:
        for block_number in doubly_indirect_pointers:
            print >>output_file, block_number

    with open('trebly_indirect_pointers.txt', 'w'): pass
    with open('trebly_indirect_pointers.txt', 'a') as output_file:
        for block_number in trebly_indirect_pointers:
            print >>output_file, block_number
            
    with open('free_blocks.txt', 'w'): pass
    with open('free_blocks.txt', 'a') as output_file:
        for block_number in free_blocks:
            print >>output_file, block_number

    with open('directory_table.txt', 'w'): pass
    with open('directory_table.txt', 'a') as output_file:
        for directory in directory_table:
            print >>output_file, '{},'.format(directory)
            print >>output_file, str(directory_table[directory]).strip('[]')
    """
            
if __name__ == "__main__": main()
