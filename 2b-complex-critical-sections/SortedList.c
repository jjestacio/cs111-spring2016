#define _GNU_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <string.h> /*strcmp()*/
#include <stdlib.h>
#include <pthread.h>
#include "SortedList.h"

int opt_yield = 0;

/**
 * SortedList_insert ... insert an element into a sorted list
 *
 *	The specified element will be inserted in to
 *	the specified list, which will be kept sorted
 *	in ascending order based on associated keys
 *
 * @param SortedList_t *list ... header for the list
 * @param SortedListElement_t *element ... element to be added to the list
 *
 * Note: if (opt_yield & INSERT_YIELD)
 *		call pthread_yield in middle of critical section
 */
void SortedList_insert(SortedList_t *list, SortedListElement_t *element) {

  SortedListElement_t *traverse = list;
  SortedListElement_t *next = traverse->next;
  
  while(next != NULL) {
    if(strncmp(element->key, next->key, 3) <= 0)
      break;
    
    traverse = next;
    next = next->next;
  }
  
  if(opt_yield && INSERT_YIELD)
    pthread_yield();

  element->prev = traverse;
  element->next = next;
  traverse->next = element;
  if(next != NULL) // If element is to be inserted at the tail
    next->prev = element;

  //free(traverse);
  //free(next);
}

/**
 * SortedList_delete ... remove an element from a sorted list
 *
 *	The specified element will be removed from whatever
 *	list it is currently in.
 *
 *	Before doing the deletion, we check to make sure that
 *	next->prev and prev->next both point to this node
 *
 * @param SortedListElement_t *element ... element to be removed
 *
 * @return 0: element deleted successfully, 1: corrtuped prev/next pointers
 *
 * Note: if (opt_yield & DELETE_YIELD)
 *		call pthread_yield in middle of critical section
 */
int SortedList_delete(SortedListElement_t *element) {
	
  // element is the head of an empty list
  if(element->prev == NULL && element->next == NULL);
  
  // element is the head of a non empty list
  else if (element->prev == NULL) {
    if(opt_yield && DELETE_YIELD)
      pthread_yield();
    element->next->prev = element->prev;
  }

  // element is the tail of a non empty list
  else if (element->next == NULL) {
    if(opt_yield && DELETE_YIELD)
      pthread_yield();
    element->prev->next = element->next;

  }

  // element is corrupted
  else if (element->next->prev != element || element->prev->next != element)
    return 1;

  // element is somewhere in the middle
  else {
    if(opt_yield && DELETE_YIELD)
      pthread_yield();
    element->next->prev = element->prev;
    element->prev->next = element->next;
  }

  
  //element->key = NULL;
  //free((char*)element->key);
  //free(element->prev);
  //free(element->next);
  
  return 0;
  
}

/**
 * SortedList_lookup ... search sorted list for a key
 *
 *	The specified list will be searched for an
 *	element with the specified key.
 *
 * @param SortedList_t *list ... header for the list
 * @param const char * key ... the desired key
 *
 * @return pointer to matching element, or NULL if none is found
 *
 * Note: if (opt_yield & SEARCH_YIELD)
 *		call pthread_yield in middle of critical section
 */
SortedListElement_t *SortedList_lookup(SortedList_t *list, const char *key) {
  
  SortedListElement_t *traverse = list;
  while(traverse != NULL) {
    if(traverse->key == key)
      return traverse;
    traverse = traverse->next;
  }

  //free(traverse);
  return NULL;
}

/**
 * SortedList_length ... count elements in a sorted list
 *	While enumeratign list, it checks all prev/next pointers
 *
 * @param SortedList_t *list ... header for the list
 *
 * @return int number of elements in list (excluding head)
 *	   -1 if the list is corrupted
 *
 * Note: if (opt_yield & SEARCH_YIELD)
 *		call pthread_yield in middle of critical section
 */
int SortedList_length(SortedList_t *list) {

  int length = 0;
  SortedListElement_t *traverse = list;
  
  while(traverse->next != NULL) {
    if(opt_yield && SEARCH_YIELD)
      pthread_yield();
    traverse = traverse->next;
    length++;
  }

  //free(traverse);
  return length;

}
