#include "SortedList.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <string.h>

/*------- lock_type -------------*/
#define NO_LOCK 1
#define MUTEX 2
#define SPIN_LOCK 3

/*------- Constants -----------------------*/
static const int KEY_LENGTH = 3;
static const int NUM_OPS = 2;

/*------- Global Variables ----------------*/
static int num_threads = 1;
static int num_iterations = 1;
static int num_elements = 1;
static struct SortedListElement **elements;
static char **keys;
static SortedList_t list = {NULL, NULL, NULL};

int lock_type = NO_LOCK;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
volatile int spin_lock;

/*------- Other Functions -----------------*/
char *rand_string();
void *thread_create();

/*------- Main ----------------------------*/
int main(int argc, char* argv[]) {

  int i; // For for loop iterations
  int j;

  /*-------- Get options ---------------------*/

  int option;
  int option_index;
  
  static struct option long_options[] = {
    {"threads", required_argument, NULL, 't'},
    {"iterations", required_argument, NULL, 'i'},
    {"yield", required_argument, NULL, 'y'},
    {"sync", required_argument, NULL, 's'}
  };
    
  while((option = getopt_long(argc, argv, "", long_options, &option_index)) != EOF)
    switch(option) {
      
    case 't':
      if((num_threads = atoi(optarg)) == 0 || num_threads <= 0) {
	fprintf(stderr, "ERROR: could not convert num_threads option argument into integer or argument given is <= 0.\n");
	exit(EXIT_FAILURE);
      }
      break;
      
    case 'i':
      if((num_iterations = atoi(optarg)) == 0 || num_iterations <= 0) {
	fprintf(stderr, "ERROR: could not convert num_iterations option argument into integer or argument given is <= 0.\n");
	exit(EXIT_FAILURE);
      }
      break;
      
    case 'y':
      i = 0;
      while(optarg[i] != '\000') {
	if(i >= 2) {
	  fprintf(stderr, "ERROR: --yield not given a correct combination of arguments.\n");
	  exit(EXIT_FAILURE);
	}
	else if(optarg[i] == 'i')
	  opt_yield |= INSERT_YIELD;
	else if(optarg[i] == 'd')
	  opt_yield |= DELETE_YIELD;
	else if(optarg[i] == 's')
	  opt_yield |= SEARCH_YIELD;
	else {
	  fprintf(stderr, "ERROR: --yield not given a correct combination of arguments.\n");
	  exit(EXIT_FAILURE);
	}
	i++;
      }

      //fprintf(stderr, "yield=%d\n", opt_yield);
      break;

    case 's':
      if(optarg[1] != '\000') {
	fprintf(stderr, "ERROR: invalid argument for --sync given.\n");
	exit(EXIT_FAILURE);
      }
      else if(optarg[0] == 'm')
	lock_type = MUTEX;
      else if (optarg[0] == 's')
	lock_type = SPIN_LOCK;
      else {
	fprintf(stderr, "ERROR: invalid argument for --sync given.\n");
	exit(EXIT_FAILURE);
      }
      break;
    }
  
  /*-------------- Initialize Elements -------------------*/
  num_elements = num_threads * num_iterations;
  elements = malloc(num_elements * sizeof(struct SortedListElement));
  keys = malloc(num_elements * sizeof(char *));
  
  for(i = 0; i < num_elements; i++) {

    keys[i] = malloc(KEY_LENGTH * sizeof *keys[i]);
    
    for(j = 0; j < KEY_LENGTH; j++)
      keys[i][j] = (char)((rand() % 93) + 33);

    elements[i] = malloc(sizeof(struct SortedListElement));
    elements[i]->next = NULL;
    elements[i]->prev = NULL;
    elements[i]->key = keys[i];
  }

  /*-------------- Run Threads ----------------------------*/
  struct timespec start_time;
  clock_gettime(CLOCK_MONOTONIC, &start_time);

  pthread_t thread[num_threads];
  for(i = 0; i < num_threads; i++) {

    int *arg = malloc(sizeof(*arg));
    *arg = i;
    
    if(pthread_create(&thread[i], NULL, &thread_create, arg) != EXIT_SUCCESS) {
      fprintf(stderr, "ERROR: could not create thread[%d].\n", i);
      exit(EXIT_FAILURE);
    }
  }

  for(i = 0; i < num_threads; i++)
    if(pthread_join(thread[i], NULL) != EXIT_SUCCESS) {
      fprintf(stderr, "ERROR: could not join thread[%d].\n", i);
      exit(EXIT_FAILURE);
      }
  
  struct timespec end_time;
  clock_gettime(CLOCK_MONOTONIC, &end_time);

  int list_length = SortedList_length(&list);
  if(list_length != 0)
    fprintf(stderr, "ERROR: length = %d\n", list_length);

  int num_ops = num_threads * num_iterations * NUM_OPS;
  fprintf(stdout, "%d threads x %d iterations x (insert + lookup/delete) = %d operations\n", num_threads, num_iterations, num_ops);

  long long elapsed_time = (end_time.tv_sec - start_time.tv_sec) * 1000000000 + end_time.tv_nsec - start_time.tv_nsec;
  fprintf(stdout, "elapsed time: %lldns\n", elapsed_time);  

  long long avg_time_per_op = elapsed_time / (long long) num_ops;
  //double correct_cost_per_op = (double) avg_time_per_op / (double) num_elements;
  fprintf(stdout, "per operation: %lldns\n", avg_time_per_op);

  exit(EXIT_SUCCESS);
}

/*------- Other Functions' Declarations -----------------*/
void *thread_create(void *thread_index) {

  int start_index = *((int *)thread_index) * num_iterations;
  int i;
  int RC;
  //int list_length;
  SortedListElement_t *current;

  // Insert elements
  for(i = start_index; i < start_index + num_iterations; i++){
    switch(lock_type) {
    case NO_LOCK:
      SortedList_insert(&list, elements[i]);
      break;
    case MUTEX:
      pthread_mutex_lock(&lock);
      SortedList_insert(&list, elements[i]);
      pthread_mutex_unlock(&lock);
      break;
    case SPIN_LOCK:
      while(__sync_lock_test_and_set(&spin_lock, 1));
      SortedList_insert(&list, elements[i]);
      __sync_lock_release(&spin_lock);
      break;
    }
  }

  // Get list length
  switch(lock_type) {
  case NO_LOCK:
    SortedList_length(&list);
    break;
  case MUTEX:
    pthread_mutex_lock(&lock);
    SortedList_length(&list);
    pthread_mutex_unlock(&lock);
    break;
  case SPIN_LOCK:
    while(__sync_lock_test_and_set(&spin_lock, 1));
    SortedList_length(&list);
    __sync_lock_release(&spin_lock);
    break;
  }

  // Delete elements
  for(i = start_index; i < start_index + num_iterations; i++){
    switch(lock_type) {
    case NO_LOCK:
      current = SortedList_lookup(&list, elements[i]->key);
      if((RC = SortedList_delete(current)) != 0)
	fprintf(stderr, "ERROR: element not deleted successfully.");
      break;
    case MUTEX:
      pthread_mutex_lock(&lock);
      current = SortedList_lookup(&list, elements[i]->key);
      if((RC = SortedList_delete(current)) != 0)
	fprintf(stderr, "ERROR: element not deleted successfully.");
      pthread_mutex_unlock(&lock);
      break;
    case SPIN_LOCK:
      while(__sync_lock_test_and_set(&spin_lock, 1));
      current = SortedList_lookup(&list, elements[i]->key);
      if((RC = SortedList_delete(current)) != 0)
	fprintf(stderr, "ERROR: element not deleted successfully.");
      __sync_lock_release(&spin_lock);
      break;
    }
  }
  
  return NULL;
}
