#include <unistd.h>/*termios(), pipe(), isatty(), read(), write(), exec()*/
#include <stdbool.h> /*booleans*/
#include <termios.h> /*termios()*/
#include <stdio.h> /*print(), getc()*/
#include <stdlib.h> /*exit()*/
#include <signal.h> /*signal()*/
#include <string.h> /*strerror()*/
#include <errno.h> /*errno()*/
#include <strings.h> /*bcopy()*/
#include <getopt.h> /*getopt_long()*/
#include <sys/types.h>
#include <sys/wait.h> /*wait()*/
#include <sys/socket.h> /*socket()*/
#include <sys/stat.h> /*open()*/
#include <fcntl.h>
#include <netinet/in.h> /*internet domain addresses*/
#include <arpa/inet.h> /*inet_addr*/
#include <netdb.h> /*hostent*/
#include <pthread.h> /*pthread()*/
#include <mcrypt.h> /*mcrypt library*/

/*------ Constants -------------------*/

// Errors
static const int ERROR = -1;
static const int RECEIVED_D = 0;
static const int RECEIVED_EOF_READERROR = 1;
static const int RECEIVED_EOF_SIGPIPE = 2;

// Reading
static const int NREAD = 1;
static const char LOG_RECEIVED_PREFIX[] = "RECEIVED 1 bytes: ";
static const int RECEIVED_PREFIX_LENGTH = 18;
static const char LOG_SENT_PREFIX[] = "SENT 1 bytes: ";
static const int SENT_PREFIX_LENGTH = 14;

// Chars
static const char INT = 0x3;
static const char EOT = 0x4;
static const char CR = 0xD;
static const char LF = 0xA;

// Misc
static const int KEY_SIZE = 16; //128 bits
static const char LOCALHOST[] = "127.0.0.1";

/*------ Global Variables ------------*/

// Encryption
bool encrypt_on = false;
MCRYPT td;
int key_fd = -1;
char *key;
char *IV;

// Logging
bool log_on = false;
int log_fd = -1;

// Misc
int RC = EXIT_SUCCESS;
int process_id = -1; /*Server: process_id > 0; Shell: process_id == 0*/

/*------ Terminal Setup --------------*/
struct termios orig_term_attr, new_term_attr;

/*------ Server/Client Variables -----*/
int port_no;
int socket_fd;
struct sockaddr_in server_addr;

/*------ Extra Functions --------------*/
void init_mcrypt();
void restore_orig_term_attr(void);
void init_new_term(void);
void signal_handler(int signum);
void *process_server_output(void *read_from_server);

/*------ Main ------------------------*/

int main(int argc, char *argv[]) {

  init_new_term();

  /*-------- Get options ---------------------*/

  int option;
  int option_index;
  
  static struct option long_options[] = {
    {"log", required_argument, NULL, 'l'},
    {"encrypt", no_argument, NULL, 'e'},
    {"port", required_argument, NULL, 'p'}
  };
    
  while((option = getopt_long(argc, argv, "", long_options, &option_index)) != EOF)
    switch(option) {
    case 'p':
      port_no = *((int *)optarg);
      break;
    case 'l':
      log_on = true;
      if((log_fd = dup(open(optarg, O_CREAT|O_TRUNC|O_WRONLY, 0644))) == ERROR) {
	fprintf(stderr, "Error: %s\n", strerror(errno));
	exit(RC=EXIT_FAILURE);
      }
      break;
    case 'e':
      encrypt_on = true;
      break;
    }
  
  /*--------- Setup client --------------------*/

  if((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == ERROR) {
    fprintf(stderr, "Error: could not create server_socket_fd.\n");
  }

  /*--------- Setup server client will be connecting to ----------*/

  server_addr.sin_addr.s_addr = inet_addr(LOCALHOST);
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port_no);
 

  /*--------- Connecting to server --------------*/

  fprintf(stdout, "Waiting to connect to server...\n");
  
  if(connect(socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == ERROR) {
    fprintf(stderr, "Error: could not connect to server.\n");
    exit(RC=EXIT_FAILURE);
  }

  fprintf(stdout, "Connected to %s on port %d!\n", LOCALHOST, port_no);

  /*--------- Process server output ---------------*/

  pthread_t process_server_thread;
  pthread_create(&process_server_thread, NULL, &process_server_output, &socket_fd);

  /*--------- Initialize mcrypt -------------------*/

  if(encrypt_on)
    init_mcrypt();
	
  /*--------- Get input from terminal -------------*/

  char buffer;
  while(1) {

    // Read error
    if(read(STDIN_FILENO, &buffer, NREAD) == 0) {
      fprintf(stderr, "Error: could not read c from stdin.\n");
      close(socket_fd); 
      exit(RC=RECEIVED_EOF_READERROR);
    }
	
    // buffer == ^D
    if(buffer == EOT) {
      fprintf(stdout, "Recieved ^D.\n");
      close(socket_fd);
      exit(RC=RECEIVED_D);
    }
    
    /*--- Map <cr> or <lf> to <cr><lf> ------*/
    else if(buffer == CR || buffer == LF) {

      // Echo to stdout
      if(write(STDOUT_FILENO, &CR, 1) != 1) {
        fprintf(stderr, "Error: could not write <cr> to stdout.\n");
        exit(RC=EXIT_FAILURE);
      }

      if(write(STDOUT_FILENO, &LF, 1) != 1) {
        fprintf(stderr, "Error: could not write <lf> to stdout.\n");
        exit(RC=EXIT_FAILURE);
      }

      /*--- Write <cr><lf>, <cr>, <lf> to server as <lf> ---*/
      buffer = LF;

      // Encrypt
      if(encrypt_on && mcrypt_generic(td, &buffer, NREAD) != 0) {
	fprintf(stderr, "Error: encryption to write to server failed.\n");
	exit(RC=EXIT_FAILURE);
      }

      // Write to server
      if(write(socket_fd, &buffer, 1) != 1) {
	fprintf(stderr, "Error: could not write <lf> to server.\n");
	exit(RC=EXIT_FAILURE);
      }
    }
    
    // All other characters
    else {

      // Echo to stdout
      if(write(STDOUT_FILENO, &buffer, NREAD) != NREAD) {
	fprintf(stderr, "Error: could not write buffer to stdout.\n");
	exit(RC=EXIT_FAILURE);
      }

      // Encrypt
      if(encrypt_on && mcrypt_generic(td, &buffer, NREAD) != 0) {
	fprintf(stderr, "Error: encryption to write to server failed.\n");
	exit(RC=EXIT_FAILURE);
      }

      // Write to server
      if(write(socket_fd, &buffer, NREAD) != NREAD) {
	fprintf(stderr, "Error: could not write buffer to server.\n");
	exit(RC=EXIT_FAILURE);
      }
    }

    // Write to log
    if(log_on) {
      write(log_fd, &LOG_SENT_PREFIX, SENT_PREFIX_LENGTH);
      write(log_fd, &buffer, NREAD);
      write(log_fd, &LF, 1);
    }
  }
  
  exit(RC=EXIT_SUCCESS);
}

/*---- Extra Functions' Definitions ------------*/

void restore_orig_term_attr(void) {
  
  tcsetattr(STDIN_FILENO, TCSANOW, &orig_term_attr);
  fprintf(stdout, "Restored original terminal attributes.\n");
}

void init_new_term(void) {

  // Check if a terminal
  if(!isatty(STDIN_FILENO)) {
    fprintf(stderr, "Error: STDIN_FILENO is not a terminal.\n");
    exit(EXIT_FAILURE);
  }

  // Saving original attributes
  if(tcgetattr(STDIN_FILENO, &orig_term_attr) == -1) {
    fprintf(stderr, "Error: could not get original terminal attributes.\n");
    exit(EXIT_FAILURE);
  }

  // Preparing to set new attributes
  if(tcgetattr(STDIN_FILENO, &new_term_attr) == -1) {
    fprintf(stderr, "Error: could not get new terminal attributes\n.\n");
    exit(EXIT_FAILURE);
  }

  // local modes; non-canonical, no echo input
  new_term_attr.c_lflag &= ~(ICANON|ECHO);
  new_term_attr.c_cc[VMIN] = 1;

  if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &new_term_attr) == -1) {
    fprintf(stderr, "Error: could not set new terminal attributes\n.\n");
    exit(EXIT_FAILURE);
  }

  atexit(restore_orig_term_attr);
}

void signal_handler(int signum) {

  if(signum == SIGPIPE) {
    fprintf(stderr, "Error: EOF or SIGPIPE received from shell to server.\n");
    close(socket_fd);
    kill(process_id, SIGTERM);
    exit(RC=RECEIVED_EOF_SIGPIPE);
  }
}

void *process_server_output(void *read_from_server) {

  char buffer;
  while(1) {

    // EOF read from server, so exit
    if(read(*((int *)read_from_server), &buffer, NREAD) == 0)
      signal(SIGPIPE, signal_handler);

    // Write encrypted server output to log
    if(log_on) {
      write(log_fd, &LOG_RECEIVED_PREFIX, RECEIVED_PREFIX_LENGTH);
      write(log_fd, &buffer, NREAD);
      write(log_fd, &LF, 1);
    }

    // Decrypt server output
    if(encrypt_on && mdecrypt_generic(td, &buffer, NREAD)) {
      fprintf(stderr, "Error: could not decrypt from server.\n");
      exit(RC=EXIT_FAILURE);
    }

    // Write server output to stdout
    write(STDOUT_FILENO, &buffer, NREAD);
  }
  
  return NULL;
}

void init_mcrypt() {

  // Open encryption module
  if((td = mcrypt_module_open("twofish", NULL, "cfb", NULL)) == MCRYPT_FAILED) {
    fprintf(stderr, "Error: could not open mcrypt library.\n");
    exit(RC=EXIT_FAILURE);
  }

  // Open file containing key for reading
  if((key_fd = open("my.key", O_RDONLY)) == ERROR) {
    fprintf(stderr, "Error: could not open my.key.\n");
    exit(RC=EXIT_FAILURE);
  }

  // Allocate space for key
  if((key = calloc(1, KEY_SIZE)) == NULL) {
    fprintf(stderr, "Error: could not calloc() for key.\n");
    exit(RC=EXIT_FAILURE);
  }

  // Read in key
  if(read(key_fd, key, KEY_SIZE) != KEY_SIZE) {
    fprintf(stderr, "Error: could not read from my.key into key.\n");
    exit(RC=EXIT_FAILURE);
  }

  // Close file
  if(close(key_fd) == ERROR) {
    fprintf(stderr, "Error: could not close my.key.\n");
    exit(RC=EXIT_FAILURE);
  }

  /*---- Create actual key --------------*/
  IV = malloc(mcrypt_enc_get_iv_size(td));

  int i;
  for(i = 0; i < mcrypt_enc_get_iv_size(td); i++)
    IV[i] = rand(); // Used the same seed to produce the same outputs for testing purposes
  
  if(mcrypt_generic_init(td, key, KEY_SIZE, IV) < 0) {
    fprintf(stderr, "Error: could not mcrypt_generic_init.\n");
    exit(RC=EXIT_FAILURE);
  }
}
