#include <stdio.h>
#include <unistd.h> /*exit()*/
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h> /*wait()*/
#include <sys/socket.h> /*socket()*/
#include <netinet/in.h> /*internet domain addresses*/
#include <arpa/inet.h>
#include <getopt.h> /*getopt_long()*/
#include <pthread.h> /*pthread()*/
#include <mcrypt.h> /*mycrpt library*/
#include <stdbool.h>
#include <fcntl.h> /*open()*/

/*------ Constants -------------------*/
static const char LOCALHOST[] = "127.0.0.1";
static const int ERROR = -1;
static const int NREAD = 1;
static const int RECEIVED_EOF_READERROR = 1;
static const int RECEIVED_EOF_SIGPIPE = 2;
static const int KEY_SIZE = 16; //128 bits

/*------ Global Variables ------------*/
int RC = EXIT_SUCCESS;
int process_id = -1; /*Server: process_id > 0; Shell: process_id == 0*/
bool encrypt_on = false;
MCRYPT td;
int key_fd = -1;
char *key;
char *IV;

/*------ Server/Client Variables -----*/
int port_no;
int orig_socket_fd, new_socket_fd;
struct sockaddr_in server_addr, client_addr;
socklen_t client_addr_size;

/*------ Extra Functions --------------*/
void signal_handler(int signum);
void *process_shell_output(void *read_from_shell);
void init_mcrypt();

/*------ Main ------------------------*/
int main (int argc, char *argv[]) {

  int option;
  int option_index;

  /*--------- Get options ----------------*/
  
  static struct option long_options[] = {
    {"encrypt", no_argument, NULL, 'e'},
    {"portno", required_argument, NULL, 'p'}
  };

  while((option = getopt_long(argc, argv, "", long_options, &option_index)) != EOF)
    switch(option) {
    case 'p':
      port_no = *((int *)optarg);
      break;
    case 'e':
      encrypt_on = true;
      break;
    }
  /*--------- Initialize Pipes ----------------*/

  int pipe_to_shell[2];
  int pipe_from_shell[2];

  if(pipe(pipe_to_shell) == -1 || pipe(pipe_from_shell) == -1) {
    fprintf(stderr, "Error: creating pipes.\n");
    exit(RC=EXIT_FAILURE);
  }

  /*--------- Setup server --------------------*/

  if((orig_socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == ERROR) {
    fprintf(stderr, "Error: could not create orig_socket_fd.\n");
    exit(RC=EXIT_FAILURE);
  }
  
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port_no);
  server_addr.sin_addr.s_addr = inet_addr(LOCALHOST);

  if(bind(orig_socket_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) == ERROR) {
    fprintf(stderr, "Error: could not bind server_addr to orig_socket_fd.\n");
    exit(RC=EXIT_FAILURE);
  }

  /*------- Wait for a client to connect -----------*/

  fprintf(stdout, "Waiting for client ...\n");

  listen(orig_socket_fd, 5);

  client_addr_size = sizeof(client_addr);
  if((new_socket_fd = accept(orig_socket_fd, (struct sockaddr *) &client_addr, &client_addr_size)) == ERROR) {
    fprintf(stderr, "Error: could not accept client connection request.\n");
    exit(RC=EXIT_FAILURE);
  }

  fprintf(stdout, "Client received!\n");

  /*------ Initialize mcrypt -------------*/

  if(encrypt_on)
    init_mcrypt();

  /*------ Execute a shell to process client's commands ------------------*/

  if((process_id = fork()) == -1) {
    fprintf(stderr, "Error: could not create child process.\n");
    exit(EXIT_FAILURE);
  }

  if(process_id == 0) { /*Shell*/
    
    close(pipe_to_shell[1]); /*Close write end to server*/
    close(pipe_from_shell[0]); /*Close read end from shell*/
    dup2(pipe_to_shell[0], STDIN_FILENO); /*Make shell's stdin input from server*/
    dup2(pipe_from_shell[1], STDOUT_FILENO); /*Make shell'stdout output to server*/
    close(pipe_to_shell[0]); /*Close oldfd for shell's stdin*/
    close(pipe_from_shell[1]); /*Close oldfd for shell's stdout*/

    char *shell_args[2];
    shell_args[0] = "/bin/bash";
    shell_args[1] = NULL;
    
    if(execvp(shell_args[0], shell_args) == -1) {
      fprintf(stderr, "Error: could not launch shell.\n");
      exit(RC=EXIT_FAILURE);
    }
  }

  else { /*Server*/

    pthread_t process_shell_thread;
    pthread_create(&process_shell_thread, NULL, &process_shell_output, &pipe_from_shell[0]);
    
    close(pipe_to_shell[0]); /*Close read end to shell*/
    close(pipe_from_shell[1]); /*Close write end from shell*/

    /*-- Redirect server stdin, stdout, and stderr to client --*/
    dup2(new_socket_fd, STDIN_FILENO);
    dup2(new_socket_fd, STDOUT_FILENO);
    dup2(new_socket_fd, STDERR_FILENO);
    close(new_socket_fd);

    /*------ Read from client and write to shell ---------------------*/
    char buffer;
    while(1) {
      if(read(STDIN_FILENO, &buffer, NREAD) <= 0) {
	fprintf(stderr, "Error: EOF or read error received from client.\n");
	close(new_socket_fd);
	kill(process_id, SIGTERM);
	exit(RC=RECEIVED_EOF_READERROR);
      }

      if(encrypt_on && mdecrypt_generic(td, &buffer, NREAD) != 0) {
	fprintf(stderr, "Error: could not decrypt from client.\n");
	exit(RC=EXIT_FAILURE);
      }

      if(write(pipe_to_shell[1], &buffer, NREAD) != NREAD) {
	fprintf(stderr, "Error: could not write buffer to pipe_to_shell[1].\n");
	exit(RC=EXIT_FAILURE);
      }
    }
  }

  int status;
  waitpid(process_id, &status, 0);

  if(WIFSIGNALED(status)) {
    fprintf(stderr, "Shell terminated by signal: %d.\n", WTERMSIG(status));
    exit(RC = WEXITSTATUS(status));
  }
  
  exit(RC=EXIT_SUCCESS);
}

/*------ Extra Functions Declarations --------------*/

void signal_handler(int signum) {

  if(signum == SIGPIPE) {
    fprintf(stderr, "Error: EOF or SIGPIPE received from shell to server.\n");
    close(new_socket_fd);
    kill(process_id, SIGTERM);
    exit(RC=RECEIVED_EOF_SIGPIPE);
  }
}

void *process_shell_output(void *read_from_shell) {

  char buffer;
  while(1) {

    read(*((int *)read_from_shell), &buffer, NREAD);

    /*Server receives an EOF from pipe_from_shell[0]*/
    if(buffer == 0)
      signal(SIGPIPE, signal_handler);

    if(encrypt_on && mcrypt_generic(td, &buffer, NREAD) != 0) {
      fprintf(stderr, "Error: encryption to write to client failed.\n");
      exit(RC=EXIT_FAILURE);
    }
    
    write(STDOUT_FILENO, &buffer, NREAD);
  }
  
  return NULL;
}

void init_mcrypt() {

  if((td = mcrypt_module_open("twofish", NULL, "cfb", NULL)) == MCRYPT_FAILED) {
    fprintf(stderr, "Error: could not open mcrypt library.\n");
    exit(RC=EXIT_FAILURE);
  }

  if((key_fd = open("my.key", O_RDONLY)) == ERROR) {
    fprintf(stderr, "Error: could not open my.key.\n");
    exit(RC=EXIT_FAILURE);
  }

  if((key = calloc(1, KEY_SIZE)) == NULL) {
    fprintf(stderr, "Error: could not calloc() for key.\n");
    exit(RC=EXIT_FAILURE);
  }

  if(read(key_fd, key, KEY_SIZE) != KEY_SIZE) {
    fprintf(stderr, "Error: could not read from my.key into key.\n");
    exit(RC=EXIT_FAILURE);
  }

  if(close(key_fd) == ERROR) {
    fprintf(stderr, "Error: could not close my.key.\n");
    exit(RC=EXIT_FAILURE);
  }

  IV = malloc(mcrypt_enc_get_iv_size(td));

  int i;
  for(i = 0; i < mcrypt_enc_get_iv_size(td); i++)
    IV[i] = rand();

  if(mcrypt_generic_init(td, key, KEY_SIZE, IV) < 0) {
    fprintf(stderr, "Error: could not mcrypt_generic_init.\n");
    exit(RC=EXIT_FAILURE);
  }
}
