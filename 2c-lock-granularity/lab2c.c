#include "SortedList.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <string.h>

/*------- lock_type -------------*/
#define NO_LOCK 1
#define MUTEX 2
#define SPIN_LOCK 3

/*------- Constants -----------------------*/
static const int KEY_LENGTH = 3;
static const int NUM_OPS = 2;

/*------- Global Variables ----------------*/
static int num_threads = 1;
static int num_iterations = 1;
static int num_elements = 1;
static int num_lists = 1;
static struct SortedListElement **elements;
static int *which_list;
static char **keys;

static int lock_type = NO_LOCK;
static pthread_mutex_t global_lock = PTHREAD_MUTEX_INITIALIZER;

/* SINGLE LIST
static SortedList_t list = {NULL, NULL, NULL};
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
volatile int spin_lock;
*/

// SUB LISTS
static SortedList_t **lists;
static pthread_mutex_t *locks;
volatile int *spin_locks;

/*------- Other Functions -----------------*/
char *rand_string();
void *thread_create();

/*------- Main ----------------------------*/
int main(int argc, char* argv[]) {

  int i; // For for loop iterations
  int j;

  /*-------- Get options ---------------------*/

  int option;
  int option_index;
  
  static struct option long_options[] = {
    {"threads", required_argument, NULL, 't'},
    {"iterations", required_argument, NULL, 'i'},
    {"yield", required_argument, NULL, 'y'},
    {"sync", required_argument, NULL, 's'},
    {"lists", required_argument, NULL, 'l'}
  };
    
  while((option = getopt_long(argc, argv, "", long_options, &option_index)) != EOF)
    switch(option) {
      
    case 't':
      if((num_threads = atoi(optarg)) == 0 || num_threads <= 0) {
	fprintf(stderr, "ERROR: could not convert num_threads option argument into integer or argument given is <= 0.\n");
	exit(EXIT_FAILURE);
      }
      break;
      
    case 'i':
      if((num_iterations = atoi(optarg)) == 0 || num_iterations <= 0) {
	fprintf(stderr, "ERROR: could not convert num_iterations option argument into integer or argument given is <= 0.\n");
	exit(EXIT_FAILURE);
      }
      break;
      
    case 'y':
      i = 0;
      while(optarg[i] != '\000') {
	if(i >= 2) {locks = malloc(num_lists * sizeof(pthread_mutex_t));
	  fprintf(stderr, "ERROR: --yield not given a correct combination of arguments.\n");
	  exit(EXIT_FAILURE);
	}
	else if(optarg[i] == 'i')
	  opt_yield |= INSERT_YIELD;
	else if(optarg[i] == 'd')
	  opt_yield |= DELETE_YIELD;
	else if(optarg[i] == 's')
	  opt_yield |= SEARCH_YIELD;
	else {
	  fprintf(stderr, "ERROR: --yield not given a correct combination of arguments.\n");
	  exit(EXIT_FAILURE);
	}
	i++;
      }

      //fprintf(stderr, "yield=%d\n", opt_yield);
      break;

    case 's':
      if(optarg[1] != '\000') {
	fprintf(stderr, "ERROR: invalid argument for --sync given.\n");
	exit(EXIT_FAILURE);
      }
      else if(optarg[0] == 'm')
	lock_type = MUTEX;
      else if (optarg[0] == 's')
	lock_type = SPIN_LOCK;
      else {
	fprintf(stderr, "ERROR: invalid argument for --sync given.\n");
	exit(EXIT_FAILURE);
      }
      break;

    case 'l':
      if((num_lists = atoi(optarg)) <= 0) {
	fprintf(stderr, "ERROR: --lists must have an argument greater than zero.");
	exit(EXIT_FAILURE);
      }
    }
  
  /*-------------- Initialize Elements -------------------*/
  num_elements = num_threads * num_iterations;
  elements = malloc(num_elements * sizeof(struct SortedListElement));
  keys = malloc(num_elements * sizeof(char *));
  which_list = malloc(num_elements * sizeof(int *));
  
  for(i = 0; i < num_elements; i++) {

    keys[i] = malloc(KEY_LENGTH * sizeof *keys[i]);
    
    for(j = 0; j < KEY_LENGTH; j++)
      keys[i][j] = (char)((rand() % 255));

    // Which list key belongs to ; KEY_LENGTH == 3
    which_list[i] = abs(keys[i][0] * keys[i][1] * keys[i][2] % num_lists);
    //fprintf(stderr, "key %d belongs to list %d\n", i, which_list[i]);

    elements[i] = malloc(sizeof(struct SortedListElement));
    elements[i]->next = NULL;
    elements[i]->prev = NULL;
    elements[i]->key = keys[i];
  }

  /*-------------- Initialize Lists -----------------------*/
  lists = malloc(num_lists * sizeof(struct SortedListElement));

  for(i = 0; i < num_lists; i++) {
    lists[i] = malloc(sizeof(struct SortedListElement));
    lists[i]->next = NULL;
    lists[i]->prev = NULL;
    lists[i]->key = NULL;
  }

  /*------------- Initialize locks/spin-locks -------------*/
  if(lock_type == NO_LOCK);
  else if(lock_type == SPIN_LOCK) spin_locks = malloc(num_lists * sizeof(int));
  else { // lock_type == MUTEX
    locks = malloc(num_lists * sizeof(pthread_mutex_t));
    for(i = 0; i < num_lists; i++)
      pthread_mutex_init(&locks[i], NULL);
  }

  /*-------------- Run Threads ----------------------------*/
  struct timespec start_time;
  clock_gettime(CLOCK_MONOTONIC, &start_time);

  pthread_t thread[num_threads];
  for(i = 0; i < num_threads; i++) {

    int *arg = malloc(sizeof(*arg));
    *arg = i;
    
    if(pthread_create(&thread[i], NULL, &thread_create, arg) != EXIT_SUCCESS) {
      fprintf(stderr, "ERROR: could not create thread[%d].\n", i);
      exit(EXIT_FAILURE);
    }
  }

  for(i = 0; i < num_threads; i++)
    if(pthread_join(thread[i], NULL) != EXIT_SUCCESS) {
      fprintf(stderr, "ERROR: could not join thread[%d].\n", i);
      exit(EXIT_FAILURE);
      }
  
  struct timespec end_time;
  clock_gettime(CLOCK_MONOTONIC, &end_time);

  int list_length = 0;
  for(i = 0; i < num_lists; i++)
    list_length += SortedList_length(lists[which_list[i]]);

  if(list_length != 0)
    fprintf(stderr, "ERROR: length = %d\n", list_length);

  int num_ops = num_threads * num_iterations * NUM_OPS;
  fprintf(stdout, "%d threads x %d iterations x (insert + lookup/delete) = %d operations\n", num_threads, num_iterations, num_ops);

  long long elapsed_time = (end_time.tv_sec - start_time.tv_sec) * 1000000000 + end_time.tv_nsec - start_time.tv_nsec;
  fprintf(stdout, "elapsed time: %lldns\n", elapsed_time);  

  long long avg_time_per_op = elapsed_time / (long long) num_ops;
  fprintf(stdout, "per operation: %lldns\n", avg_time_per_op);
  //double correct_cost_per_op = (double) avg_time_per_op / (double) num_elements;
  //fprintf(stdout, "corrected cost per operation: %fns\n", correct_cost_per_op);

  exit(EXIT_SUCCESS);
}

/*------- Other Functions' Declarations -----------------*/
void *thread_create(void *thread_index) {

  int start_index = *((int *)thread_index) * num_iterations;
  int i;
  int RC;
  //int list_length;
  SortedListElement_t *current;

  // Insert elements
  for(i = start_index; i < start_index + num_iterations; i++){
    switch(lock_type) {
    case NO_LOCK:
      SortedList_insert(lists[which_list[i]], elements[i]);
      break;
    case MUTEX:
      pthread_mutex_lock(&locks[which_list[i]]);
      SortedList_insert(lists[which_list[i]], elements[i]);
      pthread_mutex_unlock(&locks[which_list[i]]);
      break;
    case SPIN_LOCK:
      while(__sync_lock_test_and_set(&spin_locks[which_list[i]], 1));
      SortedList_insert(lists[which_list[i]], elements[i]);
      __sync_lock_release(&spin_locks[which_list[i]]);
      break;
    }
  }
  
  // Get list length
  int list_length = 0;
  switch(lock_type) {
  case NO_LOCK:
    for(i = 0; i < num_lists; i++)
      list_length +=SortedList_length(lists[which_list[i]]);
    break;
  case MUTEX:
    for(i = 0; i < num_lists; i++) {
     pthread_mutex_lock(&global_lock);
     list_length += SortedList_length(lists[which_list[i]]);
     pthread_mutex_unlock(&global_lock);
    }
    break;
  case SPIN_LOCK:
    while(__sync_lock_test_and_set(&spin_locks[which_list[i]], 1));
    SortedList_length(lists[which_list[i]]);
    __sync_lock_release(&spin_locks[which_list[i]]);
    break;
    }

  // Delete elements
  for(i = start_index; i < start_index + num_iterations; i++){
    switch(lock_type) {
    case NO_LOCK:
      current = SortedList_lookup(lists[which_list[i]], elements[i]->key);
      if((RC = SortedList_delete(current)) != 0)
	fprintf(stderr, "ERROR: element not deleted successfully.");
      break;
    case MUTEX:
      pthread_mutex_lock(&locks[which_list[i]]);
      current = SortedList_lookup(lists[which_list[i]], elements[i]->key);
      if((RC = SortedList_delete(current)) != 0)
	fprintf(stderr, "ERROR: element not deleted successfully.");
      pthread_mutex_unlock(&locks[which_list[i]]);
      break;
    case SPIN_LOCK:
      while(__sync_lock_test_and_set(&spin_locks[which_list[i]], 1));
      current = SortedList_lookup(lists[which_list[i]], elements[i]->key);
      if((RC = SortedList_delete(current)) != 0)
	fprintf(stderr, "ERROR: element not deleted successfully.");
      __sync_lock_release(&spin_locks[which_list[i]]);
      break;
    }
  }
  
  return NULL;
}
